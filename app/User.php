<?php

namespace App;
use App\AttendeeDetails;
use App\Address;
use App\Paper;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * A User has one attendee details entry on DB.
     */
    public function attendeeDetails()
    {
        return $this->hasOne(AttendeeDetails::class);
    }

    /**
     * A user may have many addresses.
     */
    public function addresses()
    {
        return $this->belongsToMany(Address::class);
    }

    /**
     * A user has zero or more papers.
     */
    public function papers()
    {
        return $this->hasMany(Paper::class);
    }

    public function isAdmin()
    {
        // 1 is the admin user, the god/root of the admin area.
        return 'admin' == $this->role;
    }

    public function isEditor()
    {
        // 2 is the editor, can publish stuff.
        return 'editor' == $this->role;
    }

    public function isAttendee()
    {
        return 'attendee' == $this->role;
    }

    public function isVisitor()
    {
        return 'visitor' == $this->role;
    }
}
