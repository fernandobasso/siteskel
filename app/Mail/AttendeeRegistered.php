<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AttendeeRegistered extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Data comming from website form.
     *
     * @var array $data
     */
    public $data;


    /**
     * Attributes like subject and reply-to.
     *
     * @var array $attrs
     */
    protected $attrs;


    /**
     * Create a new message instance.
     *
     * @param array $data - data to send in the body.
     * @param array $attrs - things like subject and reply-to.
     *
     * @return void
     */
    public function __construct($data, $attrs)
    {
        $this->data = $data;
        $this->attrs = $attrs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject($this->attrs['subject']);

        return $this->from('naoresponda@congressomedicopassofundo.com.br', 'Congresso Médico Passo Fundo')
            ->replyTo($this->data['email'], $this->data['name'])
            ->view('emails.attendee-registered')
            ->text('emails.attendee-registered-plain');
    }
}

