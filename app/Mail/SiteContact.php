<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SiteContact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Data comming from website form.
     *
     * @var array $formData
     */
    public $formData;


    /**
     * Attributes like subject and reply-to.
     *
     * @var array $attrs
     */
    protected $attrs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($formData, array $attrs)
    {
        $this->formData = $formData;
        $this->attrs = $attrs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject($this->attrs['subject']);
        return $this->from(env('MAIL_FROM_ADDRESS'), env('APP_NAME') . ' ' . env('APP_DOMAIN'))
            ->replyTo($this->formData['email'], $this->formData['name'])
            ->view('emails.site-contact')
            ->text('emails.site-contact-plain');
    }
}

