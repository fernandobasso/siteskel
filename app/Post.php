<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
      'post_category_id',
      'title',
      'intro',
      'text'
    ];

    /**
     * Get PostCategory for this Post.
     */
    public function postCategory()
    {
        //return $this->belongsTo('App\PostCategory');
        return $this->belongsTo(PostCategory::class);
    }


    public function images()
    {
        // We always want to show images ordered by position.
        return $this->hasMany(\App\Image::class); //->orderBy('pos', 'ASC');
    }


    public function addImage($imageAttrs)
    {
        // Sets the post_id behind the scenes.
        $this->images()->create([
            'origname' => $imageAttrs->origName,
            'ext' => $imageAttrs->ext,
            // etc...
        ]);
    }

    public function isCategoryMedicos()
    {
        return 'medicos' === $this->postCategory->slug;
    }

    /**
     * Produce #t if this model is of category "Médicos"
     * and its intro field has the "|" char, like
     * "John Doe | CRM 1532".
     *
     * @return bool
     */
    public function canGetSpecialtyAndCRM(string $separator = '|')
    {
        if (! $this->isCategoryMedicos()) return false;
        return str_contains($this->intro, $separator);
    }

    public function getSpecialty()
    {
        if (! $this->canGetSpecialtyAndCRM()) {
            return 'no specialty...';
        }

        $specialty = explode('|', $this->intro)[0];
        return trim($specialty);
    }

    public function getCRM()
    {
        if (! $this->canGetSpecialtyAndCRM()) {
            return 'no crm...';
        }

        $crm = explode('|', $this->intro)[1];
        return trim($crm);
    }
}
