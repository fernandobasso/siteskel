<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

    /**
     * The upload directory for images. Not the full path. Just
     * the path relative to the root dir of the applicatio.
     *
     * @var string $_uploadDir
     */
    protected $_uploadDir;

    /**
     * That absolute path to the upload image directory.
     *
     * @var string $_uploadPath
     */
    protected $_uploadPath;

    /**
     * Indicates whether the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'post_id',
        'filename',
        'origname',
        'caption',
        'ext',
        'pos',
        'ver',
        'status'
    ];


    public function __construct(array $attrs = [])
    {
        parent::__construct($attrs);
        $this->_uploadDir = 'uploads/images';
        $this->_uploadPath = app_path($this->_uploadDir);
    }


    public function post()
    {
        return $this->belongsTo(\App\Post::class);
    }


    public function buildPos($post_id) {
        $images = \App\Image::where('post_id', $post_id)->get();
        $len = count($images);
        for ($i = 0; $i < $len; ++$i) {
            $images[$i]->update(['pos' => $i + 1]);
        }
    }


    /**
     * Composes and returns image name on disk.
     *
     * @param string $path - sm, md, lg, orig.
     * @param boolean $include_version = false.
     *
     * @return string
     */
    public function path($size = 'md', $include_version = true) {
        $str = $this->_uploadDir
            . "/{$this->post_id}-{$this->id}-{$size}.{$this->ext}";

        if (true === $include_version) {
            $str .= "?=v{$this->ver}";
        }

        return $str;
    }
}

