<?php

namespace App;

use App\Post;
use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{

    protected $fillable = [
      'name',
      'slug',
      'description'
    ];

    /**
     * Get the posts for this PostCategory.
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
