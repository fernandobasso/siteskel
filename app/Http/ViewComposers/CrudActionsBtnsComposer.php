<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\UserRepository;

class CrudActionsBtnsComposer
{
    /**
     * Array of action button default configuration.
     *
     * @var array
     */
    protected $btns;

    /**
     * Create a new CrudActionBtnsComposer composer.
     *
     * @return void
     */
    public function __construct()
    {
        $this->btns = [
            'create' => [
                'display' => false,
                'text' => 'Cadastrar',
                'route' => route('admin.welcome')
            ],
            'cancel' => [
                'display' => false,
                'text' => 'Voltar',
                'route' => back()
            ],
            'submit' => [
                'display' => true,
                'text' => 'Gravar'
            ],
        ];
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('btns', $this->btns);
    }
}
