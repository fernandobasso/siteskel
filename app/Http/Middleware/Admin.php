<?php
// utf8=✔ 👍
// filename: app/Http/Middleware/Admin.php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming requests for admin area.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && (Auth::user()->isEditor() || Auth::user()->isAdmin())) {
            return $next($request);
        }

        return redirect('/login');
    }
}
