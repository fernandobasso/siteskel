<?php

function getCurrentYear() {
    return (new DateTime('now', new DateTimeZone('America/Sao_Paulo')))->format('Y');
}

function link_active($path) {
    return Request::is($path) ? 'active' : '';
}


function downcase($str) {
    return mb_strtolower($str, 'UTF-8');
}

function upcase($str) {
    return mb_strtoupper($str, 'UTF-8');
}

/**
 * A file's name without extension.
 *
 * @param string $str
 * @return string
 */
function drop_file_ext($str) {
    return pathinfo($str, PATHINFO_FILENAME);
}

function img_post(\App\Image $img, $size = 'md') {
    return asset("uploads/images/{$img->post_id}-{$img->id}-{$size}.{$img->ext}?v={$img->ver}");
}

/**
 * Strips away anything that is not a digit.
 *
 *   '894.000.051-00' → '89400005100'
 *   '87060-071'      → '87060071'
 *   '(99) 9991-9991  → '9999919991
 *
 * @param string $val
 * @return string
 */
function only_digits($val) {
    return preg_replace('/[^0-9]/', '', $val);
}

/**
 * Parses a video id from any youtube url.
 *
 * @param string $link
 * @return string|null
 */
function video_id_youtube($link) {
    $video_id = null;
    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $link, $match)) {
        $video_id = $match[1];
    }

    return $video_id;
}
