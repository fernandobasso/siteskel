<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Address;
use App\AttendeeDetails;
use App\Mail\AttendeeRegistered;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    /**
     * Create a user of the ‘type’ attendee.
     */
    protected function createAttendee(Request $req)
    {
        $data = $req->input('attendee');
        $random_password = str_random(8);

        DB::beginTransaction();

        $attendeeDetails = new AttendeeDetails([
            'badge_name' => $data['name'],
            'cpf' => $data['cpf'],
            'phone' => $data['phone'],
            'profession' => $data['profession'],
            'attendee_category_id' => $data['category_id'],
        ]);

        $address = new Address([
          'street' => $data['street'],
          'number' => $data['number'],
          'district' => $data['district'],
          'cep' => $data['cep'],
          'state' => $data['state'],
          'city' => $data['city'],
        ]);

        try {
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($random_password),
                'role' => 'attendee',
            ]);

            $user->attendeeDetails()->save($attendeeDetails);
            $user->addresses()->save($address);

            Mail::to($data['email'])
                ->bcc([
                    // 'ameplan@ameplan.com.br',
                    'atendimento@vbsmidia.com.br',
                    'suporte@analisys.com.br',
                ])
                ->send(new AttendeeRegistered(
                    [
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'phone' => $data['phone'],
                        'password' => $random_password,
                    ],
                    [
                        'subject' => 'Inscrição no Site - congressomedicopassofundo.com.br',
                    ]
                )
            );

            if (count(Mail::failures()) > 0) {
                DB::rollBack();
                exit('email failures');
                //return redirect('/inscricoes')->with('danger', 'Erro ao fazer inscrição.');
            }

            DB::commit();

            Auth::login($user);

            return redirect('/area-do-participante')->with(
                'success',
                '<div class="text-center">Inscrição realizada com sucesso!</div>
                 <div class="text-center">Confira o seu email para saber sua senha de acesso.</div>');
        }
        catch (\Exception $err) {
            DB::rollBack();
            echo $err->getMessage();
            echo $err->getTraceAsString();
            exit();
            return redirect('/inscricoes')->with('danger', 'Erro ao fazer inscrição.');
        }
    }
}
