<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PostCategory;
use App\Post;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news_id = PostCategory::where('slug', 'novidades')->get()->first()->id;
        $news = Post::where('post_category_id', $news_id)->orderBy('created_at', 'DESC')->get();
        return view('site.news.index', [
            'news' => $news
        ]);
    }


    public function show($post_id)
    {
        $post = Post::whereKey($post_id)->with([
            'images' => function ($q) {
                $q->orderBy('pos', 'ASC');
            }
        ])->get()->first();

        // $post->title, $post->intro, etc.
        // $post->images[0], $post->images[1], and so on and so forth.

        return view('site.news.show', ['new' => $post]);
    }


    public function showTratamento($tratamento_id)
    {
        $tratamento = Post::whereKey($tratamento_id)->with([
            'images' => function ($q) {
                $q->orderBy('pos', 'ASC');
            }
        ])->get()->first();

        return view('site.tratamentos.show', ['tratamento' => $tratamento]);
    }


    public function showSobre()
    {
        $category_sobre = PostCategory::where('slug', 'sobre')->get()->first();
        $sobre = Post::where('post_category_id', $category_sobre->id)->with([
            'images' => function ($q) {
                $q->orderBy('pos', 'ASC');
            }
        ])->limit(1)->get()[0];

        return view('site.page-sobre', compact('sobre'));
    }
}
