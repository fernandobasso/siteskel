<?php

namespace App\Http\Controllers\Site;

use App\Post;
use App\PostCategory;
use App\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $news = $this->getPostsByCategorySlug('novidades', ['created_at', 'DESC'], 3);
        $about = Post::find(1);
        $services = $this->getPostsByCategorySlug('servicos', ['created_at', 'DESC'], 8);
        $banners = Banner::where('status', '1')
            ->orderBy('pos', 'ASC')
            ->get();

        return view('site.site-main-layout', compact(
            'banners',
            'news',
            'about',
            'services',
        ));
    }


    /**
     * Gets posts by the slug of a category.
     *
     * Always includes the images ordered from 1st position.
     *
     * @param string $category_slug
     *        ex: 'exams', 'news', 'treatments'.
     * @param array $order_by
     *        ex: ['id', 'DESC'], ['title', 'ASC', 'created_at', 'DESC'].
     *        $order_by HAS to always be a pair, ['column', 'direction'].
     * @param int $limit
     *        number of records to retrieve.
     * @return Collection
     */
    protected function getPostsByCategorySlug(string $category_slug, array $order_by, int $limit)
    {
        $category = PostCategory::where('slug', $category_slug)->get()->first();

        $posts = [];
        if ($category) {
            $posts = Post::where('post_category_id', $category->id)->with([
                'images' => function ($q) {
                    $q->orderBy('pos', 'ASC');
                }
            ])->orderBy($order_by[0], $order_by[1])->limit($limit)->get();
        }

        return $posts;
    }
}

