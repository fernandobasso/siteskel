<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\Mail\SiteContact;
Use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * Stores contact message.
     *
     * @param Request $req
     */
    public function create(Request $req)
    {

        $params = $this->contactParams($req);

        $res = Contact::create($params);

        try {
            Mail::to('contato@integradocontabilidade.com.br')
                ->bcc(['atendimento@vbsmidia.com.br', 'suporte@analisys.com.br'])
                ->send(new SiteContact(
                    $params,
                    [
                        'subject' => 'Contato do Site - ' . env('APP_NAME') . ' ' . env('APP_DOMAIN'),
                    ]
                )
            );
        }
        catch (\Exception $err) {
            return [
                'status' => 'danger',
                'msg' => 'Seu contato pode não ter sido enviado. Se não tiver resposta, tente novamente em alguns minutos.'
            ];
        }

        if (count(Mail::failures()) > 0) {
            return [
                'status' => 'danger',
                'msg' => 'Seu contato pode não ter sido enviado. Se não tiver resposta, tente novamente em alguns minutos.'
            ];
        }

        return [
            'status' => 'success',
            'msg' => 'Contato enviado com sucesso!'
        ];
    }


    /**
     * Returns only the allowed Contact attributes.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    protected function contactParams(Request $req)
    {
        $fillable = (new Contact())->getFillable();
        $attrs = array_reduce($fillable, function ($acc, $item) use ($req) {
            $acc[$item] = $req->input('contact')[$item];
            return $acc;
        }, []);

        return $attrs;
    }
}
