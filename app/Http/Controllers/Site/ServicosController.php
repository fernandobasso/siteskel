<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PostCategory;
use App\Post;

class ServicosController extends Controller
{
    public function show($servico_id)
    {
        $servico = Post::whereKey($servico_id)->with([
            'images' => function ($q) {
                $q->orderBy('pos', 'ASC');
            }
        ])->get()->first();

        // $post->title, $post->intro, etc.
        // $post->images[0], $post->images[1], and so on and so forth.

        return view('site.servicos.show', compact('servico'));
    }
}

