<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PostCategory;
use App\Post;

class LinksUteisController extends Controller
{
    public function index()
    {
        $category = PostCategory::where('slug', 'links-uteis')->get()->first();

        $links_uteis = [];
        if ($category) {
            //$links_uteis = Post::where('post_category_id', $category->id)
            //             ->orderBy('id', 'ASC')
            //             ->get();
            $links_uteis = Post::whereIn('id', [13, 14, 15, 16])->get();
        }

        return view('site.links-uteis.index', compact('links_uteis'));
    }
}


