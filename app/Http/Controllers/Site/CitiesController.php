<?php

namespace App\Http\Controllers\Site;

use App\City;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CitiesController extends Controller
{
    public function index(Request $req, $uf = '')
    {
        if ('' === $uf) {
            return ['no uf, no citties, sorry...'];
        }

        $cities = City::where('uf', $uf)->get();

        $opts = "<option value=''>Selecione...</option>\n";
        forEach($cities as $city) {
            $opts .= "<option value='{$city->id}'>{$city->name}</option>\n";
        }

        return response($opts, 200)->header('Content-Type', 'text/plain');
    }
}

