<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PostCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\AccessDeniedHttpException;

class PostCategoriesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('admin-can-manage-post-categories')) {
            return redirect()->route('admin.welcome');
        }

        $post_categories = PostCategory::all();
        return view('admin.post-categories.index', ['post_categories' => $post_categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('admin-can-manage-post-categories')) {
            return redirect()->route('admin.welcome');
        }

        return view('admin.post-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (Gate::denies('admin-can-manage-post-categories')) {
            return redirect()->route('admin.welcome');
        }

        $inputs = $request->all();
        PostCategory::create($inputs);
        return redirect()->route('post-categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::denies('admin-can-manage-post-categories')) {
            return redirect()->route('admin.welcome');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('admin-can-manage-post-categories')) {
            return redirect()->route('admin.welcome');
        }

        $post_category = PostCategory::find($id);
        return view('admin.post-categories.edit', ['post_category' => $post_category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('admin-can-manage-post-categories')) {
            return redirect()->route('admin.welcome');
        }

        $post_category = PostCategory::find($id);
        $post_category->update($request->all());
        return redirect()->route('post-categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin-can-manage-post-categories')) {
            return redirect()->route('admin.welcome');
        }
    }

}
