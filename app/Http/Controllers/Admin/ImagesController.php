<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Post;
use Intervention\Image\Facades\Image;
use App\Image as Img;

class ImagesController extends Controller
{

    public function create(Request $req)
    {
        $post = Post::with('postCategory')->find($req->post_id);
        return view('admin.images.images', compact('post'));
    }

    public function store(Request $req)
    {
        $file = $req->file('image');
        $post_id = $req->input('post_id');
        $origname = drop_file_ext($file->getClientOriginalName());
        $filename = str_slug($origname);
        $ext = downcase($file->getClientOriginalExtension());
        $pos = $this->maxPos($post_id);

        $img = Img::create([
            'post_id' => $post_id,
            'origname' => $origname,
            'filename' => $filename,
            'ext' => $ext,
            'pos' => $pos,
            'ver' => 1
        ]);

        $this->saveSizes($file, $img);

        return $img;
    }


    /**
     * @ajax
     *
     * Fetches images for posts/<post_id>/images page.
     *
     * @param Request $req
     * @return json
     */
    public function fetch(Request $req, $post_id)
    {
        return Img::where('post_id', $post_id)->orderBy('pos', 'ASC')->get();
    }


    /**
     * Stores image on disk.
     *
     * @param array $file - uploaded physical file.
     * @param \App\Image $img
     */
    protected function  saveSizes($file, $img)
    {
        $image_sm = Image::make($file);
        $image_md = Image::make($file);
        $image_lg = Image::make($file);
        $image_orig = Image::make($file);

        // max width 180, auto height.
        $image_sm->resize(180, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(
            sprintf('uploads/images/%d-%d-sm.%s', $img->post_id, $img->id, $img->ext)
        );

        $image_md->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(
            sprintf('uploads/images/%d-%d-md.%s', $img->post_id, $img->id, $img->ext)
        );

        $image_lg->resize(960, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(
            sprintf('uploads/images/%d-%d-lg.%s', $img->post_id, $img->id, $img->ext)
        );

        $image_orig->save(
            sprintf('uploads/images/%d-%d-orig.%s', $img->post_id, $img->id, $img->ext)
        );
    }


    public function destroy($image_id)
    {
        $img = Img::find($image_id);
        $this->destroySizes($img);
        $res = $img->delete();
        $this->_repositionAfterDestroy($img->pos, $img->post_id);

        if (true !== $res) {
            return [
                'status' => 'danger',
                'msg' => 'Erro ao remover a imagem.'
            ];
        }

        return [
            'status' => 'success',
            'msg' => 'Imagem removida com sucesso!'
        ];
    }


    /**
     * Removes files from disk.
     *
     * We don't care if the images don't exist and there is nothing to be destroyed.
     *
     * @param \App\Image
     * @return void
     */
    protected function destroySizes(\App\Image $img)
    {
        $pattern = sprintf('uploads/images/%d-%d-*-%s', $img->post_id, $img->id, $img->ext);
        array_map('unlink', glob($pattern));
    }


    /**
     * Returns the max image position for that post or 1 if null.
     *
     * @param integer $post_id
     * @return integer
     */
    protected function maxPos($post_id)
    {
        $pos = DB::table('images')->where('post_id', $post_id)->max('pos');
        return $pos ? $pos + 1 : 1;
    }



    /**
     * @ajax
     *
     * Listen to the ajax request and invoke other methods to
     * do the reordering accordingly.
     */
    public function reposition(Request $req, $post_id, $image_id)
    {
        $image_id = $req->input('imageId');
        $newpos = $req->input('newpos');
        $oldpos = $req->input('oldpos');

        $img = Img::find($image_id);

        $status = $this->_setPos($img, $oldpos, $newpos);

        if (!$status) {
            return [
                'status' => 'danger',
                'msg' => 'Erro ao reposicionar as imagens.'
            ];
        }

        return [
            'status' => 'success',
            'msg' => 'Imagens reposicionadas com sucesso!'
        ];
    }


    protected function _setPos(\App\Image $img, $oldpos, $newpos)
    {
        if ($newpos < $oldpos) {
            $res = $this->_moveUpToPos($img, $oldpos, $newpos);
        } else {
            $res = $this->_moveDownToPos($img, $oldpos, $newpos);
        }

        return ['status' => $res];
    }


    protected function _moveUpToPos(\App\Image $img, $oldpos, $newpos)
    {
        $sql = <<<EOF
            UPDATE images SET
                pos = pos + 1
                WHERE post_id = {$img->post_id}
                AND pos >= $newpos
                AND pos < $oldpos
EOF;

        DB::statement($sql);

        return $img->update(['pos' => $newpos]);
    }

    protected function _moveDownToPos(\App\Image $img, $oldpos, $newpos)
    {
        $sql = <<<EOF
            UPDATE images SET
                pos = pos - 1
                WHERE post_id = {$img->post_id}
                AND pos <= $newpos
                AND pos > $oldpos
EOF;

        DB::statement($sql);

        return $img->update(['pos' => $newpos]);
    }


    /**
     * Reposition all images from that post after the image is destroyed.
     *
     * @param int $pos - position of the destroyed image.
     * @param int $post_id
     * @return bool
     */
    protected function _repositionAfterDestroy($pos, $post_id)
    {
        $sql = <<<EOF
            UPDATE images SET
                pos = pos - 1
                WHERE post_id = $post_id
                AND pos > $pos
EOF;

        return DB::statement($sql);
    }


    public function crop(Request $req, int $post_id, int $image_id)
    {
        $img = Img::find($image_id);
        $cropData = $req->input('cropData');
        $this->_cropSizes($img, $cropData);

        $img->ver = $img->ver += 1;
        $img->save();

        return [
            'status' => 'success',
            'image' => $img
        ];
    }


    /**
     * Crop sm, md and lg image sizes.
     *
     * @param \App\Image $img
     * @param array $cropData[left, top, width, height]. All are numbers.
     */
    protected function _cropSizes(\App\Image $img, array $cropData)
    {
        // Instances of the original image without the ?v=n version thing.
        //$image_sm = Image::make($img->path('orig', false));
        //$image_md = Image::make($img->path('orig', false));
        try {
            $image = Image::make($img->path('orig', false));

            $image->crop(
            $cropData['width'],
            $cropData['height'],
            $cropData['x'],
            $cropData['y'])->save(
                sprintf('uploads/images/%d-%d-lg.%s', $img->post_id, $img->id, $img->ext)
            )->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(
                sprintf('uploads/images/%d-%d-md.%s', $img->post_id, $img->id, $img->ext)
            )->resize(180, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(
                sprintf('uploads/images/%d-%d-sm.%s', $img->post_id, $img->id, $img->ext)
            );
        }
        catch (\Exception $err) {
            echo $err->getMessage();
            return false;
        }
    }
}
