<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use App\Banner;

class BannersController extends Controller
{
    public function index(Request $req)
    {
        $banners = Banner::orderBy('status', 'DESC')->orderBy('pos', 'ASC')->get();
        return view('admin.banners.index', compact('banners'));
    }

    public function store(Request $req)
    {
        $file = null;

        $bannerAttrs = $this->bannerParams($req);

        if ($req->hasFile('banner')) {
            $file = $req->file('banner');
            $bannerAttrs['filename'] = str_slug($bannerAttrs['title']);
            $bannerAttrs['ext'] = $req->file('banner')->extension();
        }

        $banner = Banner::create($bannerAttrs);

        $img = Image::make($file);
        $img->save(
            sprintf('uploads/banners/banner%d.%s', $banner->id, $bannerAttrs['ext'])
        );

        return redirect()->route('admin.banners.index');
    }

    protected function  update(Request $req)
    {
        $bannerAttrs = $this->bannerParams($req);
        $banner = Banner::find($req->post('id'));
        $banner->update($bannerAttrs);
        return redirect()->route('admin.banners.index');
    }


    protected function destroy($id)
    {
        $banner = Banner::find($id);
        $file = sprintf('%s/uploads/banners/banner%d.%s', public_path(), $banner->id, $banner->ext);
        File::delete($file);
        $banner->delete();
        echo response()->json(['status' => 'success', 'msg' => 'Banner removido com sucesso!']);
    }


    /**
     * Returns only the allowed Post attributes.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    protected function bannerParams(Request $req)
    {
        $bannerAttrs = $req->only(
            'title',
            'subtitle',
            'link',
            'pos',
            'status',
        );

        $bannerAttrs['status'] = array_key_exists('status', $bannerAttrs) ? 1 : 0;

        return $bannerAttrs;
    }
}

