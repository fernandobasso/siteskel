<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
Use App\PostCategory;
use App\Post;

class PostsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $posts = Post::
            when($req->query('search_term'), function ($query) use ($req) {
                return $query->where('title', 'LIKE', '%' . $req->query('search_term') . '%');
            })
            ->when($req->query('category'), function ($query) use ($req) {
                // Category id 1 is the inactive category.
                return $query->where('post_category_id', $req->query('category'));
            })
            ->when($req->query('status'), function ($query) use ($req) {
                // Category id 1 is the inactive category.
                return $query->where('post_category_id', $req->query('status'));
            })
            ->with('postCategory')
            ->withCount('images')
            ->orderBy('created_at', 'DESC')
            ->get();

        $categories = PostCategory::orderBy('name', 'ASC')->get();

        return view('admin.posts.index', compact('posts', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post_categories = PostCategory::pluck('name', 'id');
        return view('admin.posts.create', ['post_categories' => $post_categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Post::create($this->postParams($request));
        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post_categories = PostCategory::pluck('name', 'id');
        $post = Post::find($id);
        return view('admin.posts.edit', [
            'post' => $post,
            'post_categories' => $post_categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Post::find($id)->update($this->postParams($request));
        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Returns only the allowed Post attributes.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    protected function postParams(Request $req)
    {
        return $req->only(
            'post_category_id',
            'title',
            'intro',
            'text'
        );
    }
}
