<?php

namespace App\Http\Controllers\Admin;

use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::all();
        return view('admin.videos.index', compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.videos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $video = Video::create($this->videoParams($request));
            return redirect()->route('videos.index')->with('success', 'Vídeo inserido com sucesso!');
        }
        catch (\Exception $err) {
            return redirect()->route('posts.index')->with('danger', 'Erro ao inserir vídeo...');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        return view('admin.videos.edit', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        try {
            Video::find($video->id)->update($this->videoParams($request));
            return redirect()->route('videos.index')->with('success', 'Vídeo atualizado com sucesso!');
        }
        catch (\Exception $err) {
            return redirect()->route('videos.index')->with('danger', 'Erro ao atualizar vídeo...');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        //
    }


    /**
     * @ajax
     *
     * Parses video ID from link/url.
     *
     * @return \Ullumiate\Http\Response
     */
    public function parseVideoId(Request $request)
    {
        $link = $request->query('link');
        if (null === $link) {
            return response([
                'msg' => 'Cannot parse link if it is missing from the request...'
            ],
            422);
        }

        return response(['video_id' => video_id_youtube($link)], 200);
    }

    /**
     * Returns only the allowed Post attributes.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    protected function videoParams(Request $request)
    {
        return $request->only(
            'title',
            'pos',
            'link',
            'intro'
        );
    }
}
