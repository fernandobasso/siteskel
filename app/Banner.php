<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{

    /**
     * The upload directory for images. Not the full path. Just
     * the path relative to the root dir of the applicatio.
     *
     * @var string $_uploadDir
     */
    protected $_uploadDir;

    /**
     * That absolute path to the upload image directory.
     *
     * @var string $_uploadPath
     */
    protected $_uploadPath;

    /**
     * Indicates whether the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $fillable = [
        'title',
        'subtitle',
        'filename',
        'link',
        'ext',
        'pos',
        'ver',
        'status'
    ];


    public function __construct(array $attrs = [])
    {
        parent::__construct($attrs);
        $this->_uploadDir = 'uploads/banners';
        $this->_uploadPath = app_path($this->_uploadDir);
    }


    /**
     * Composes and returns image name on disk.
     *
     * @param boolean $include_version = false.
     *
     * @return string
     */
    public function path($include_version = true)
    {
        $str = $this->_uploadDir
            . "/banner{$this->id}.{$this->ext}";

        if ($include_version) {
            $str .= "?=v{$this->ver}";
        }

        return $str;
    }


    public function href($className = '')
    {
        if ($this->link) {
            return "href='{$this->link}' class='{$className}'";
        }

        return '';
    }
}
