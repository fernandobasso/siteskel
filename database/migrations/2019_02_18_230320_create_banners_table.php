<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            // $table->timestamps();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->string('title', 512)->nullable();
            $table->string('subtitle', 1024)->nullable();
            $table->string('link', 1024)->nullable();
            $table->string('filename', 512);
            $table->string('ext', 4);
            $table->smallinteger('new_tab')->default(0);
            $table->smallinteger('pos')->default(0);
            $table->smallinteger('ver')->default(1);
            $table->smallinteger('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
