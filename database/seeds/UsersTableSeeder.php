<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'atendimento@vbsmidia.com.br',
            'password' => bcrypt('adm436'),
            'role' => 'admin',
            'created_at' => Carbon::now(),
        ]);
    }
}
