<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PostCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_categories')->insert([
            [
                'id' => 1,
                'name' => 'Inativos',
                'slug' => 'inativos',
                'description' => 'Conteúdos inativos (lixeira)',
                'created_at' => Carbon::now(),
            ],
            [
                'id' => 2,
                'name' => 'Sobre Nós',
                'slug' => 'sobre',
                'description' => 'Conheça mais a nossa empresa.',
                'created_at' => Carbon::now(),
            ],
            [
                'id' => 3,
                'name' => 'Novidades',
                'slug' => 'novidades',
                'description' => 'Novidades do site.',
                'created_at' => Carbon::now(),
            ],
            [
                'id' => 4,
                'name' => 'Serviços',
                'slug' => 'servicos',
                'description' => 'Serviços Oferecidos',
                'created_at' => Carbon::now(),
            ]
        ]);
    }
}
