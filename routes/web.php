<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::namespace('Site')->group(function () {

    Route::get('/', 'HomeController@home')->name('site-home');

    Route::get('/novidades', 'PostsController@index')->name('site.news.index');
    Route::get('/novidades/{post_id}/{title_slug}', 'PostsController@show')->name('site.posts.show');

    Route::get('/servicos/{servico_id}/{title_slug}', 'ServicosController@show')->name('site.services.show');

    Route::get('/sobre', 'PostsController@showSobre')->name('site.sobre.show');

    Route::get('/links-uteis', 'LinksUteisController@index')->name('site.links-uteis.index');

    Route::post('/contact', 'ContactController@create')->name('site.contact.create');
});


Route::namespace('Admin')->prefix('admin')->middleware(['auth', 'admin'])->group(function () {

    Route::get('/', function () {
        return view('admin.welcome');
    })->name('admin.welcome');

    Route::resource('posts', 'PostsController');

    Route::resource('post-categories', 'PostCategoriesController');

    Route::get(
        '/posts/{post_id}/images',
        'ImagesController@create'
    )->name('admin.images.create');

    Route::post(
        '/posts/{post_id}/images',
        'ImagesController@store'
    )->name('admin.images.store');

    // @ajax
    Route::get(
        '/posts/{post_id}/images/fetch',
        'ImagesController@fetch'
    )->name('admin.images.fetch');

    // TODO: should be /posts/{post_id}/images/{image_id} with th DELETE verb.
    Route::delete(
        '/posts/{image_id}/images',
        'ImagesController@destroy'
    )->name('admin.images.destroy');

    /**
     * @ajax
     */
    Route::patch(
        '/posts/{post_id}/images/{image_id}/reposition',
        'ImagesController@reposition'
    )->name('admin.images.reposition');

    /**
     * @ajax
     */
    Route::patch(
        '/posts/{post_id}/images/{image_id}/crop',
        'ImagesController@crop'
    )->name('admin.images.crop');


    Route::get(
        '/videos/parse-id',
        'VideosController@parseVideoId'
    )->name('admin.parse-video-id');

    Route::resource('videos', 'VideosController');


    ////////////////////////////////////////////////////////////////////////////////
    //// Banners ///////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////
    Route::get(
        '/banners',
        'BannersController@index'
    )->name('admin.banners.index');

    Route::post(
        '/banners',
        'BannersController@store'
    )->name('admin.banners.store');

    Route::patch(
        '/banners',
        'BannersController@update'
    )->name('admin.banners.update');

    Route::delete(
        '/banners/{id}',
        'BannersController@destroy'
    )->name('admin.banners.destroy');
});

