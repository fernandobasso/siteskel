
//
// This object is defined in the main site template header with the help
// of the backend language so we have some knowledge about what page/route
// we are and run some UI stuff accordingly. Look at:
//
//     resources/views/site/site-main-layout.blade.php
//
declare var SimpleCMS = {
  baseurl: String,
  routeName: String,
};

// declare var SimpleCMS: any;
