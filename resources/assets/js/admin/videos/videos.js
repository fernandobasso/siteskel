import * as $ from 'jquery';

const input = document.querySelector('.js-video-link');
const wrpPreview = document.querySelector('.js-video-preview-wrp');


const getIframe = (videoId, width = 560, height = 315) => {
  return `<iframe width="100%" height="auto" src="https://www.youtube.com/embed/${videoId}?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>`;
}

const previewVideo = (link) => {
  const getUrl = `${SimpleCMS.baseurl}/videos/parse-id`;

  $.ajax({
    url: getUrl,
    type: 'GET',
    data: { link },
    dataType: 'json',
    success: res => {
      wrpPreview.innerHTML = getIframe(res.video_id);
    },
    error(err) {
      console.error(err);
    }
  });

};

const addLinkListener = () => {
  if (input === null) {
    return;
  }

  input.addEventListener('blur', (evt) => {
    if (evt.target.value.trim().length < 1) {
      console.warn('input link empty');
      return;
    }

    previewVideo(input.value.trim());
  }, false);
};

addLinkListener();


// For when we are editing a video, preview it even without blur.
if (input.value.length > 0) {
  previewVideo(input.value.trim());
}
