import * as $ from 'jquery';

import {
  addOverlay,
  // removeOverlay,
 } from '../../shared/Helpers';

// import Cropper from 'cropperjs';

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
  }
});

/**
 * Send the file through ajax.
 *
 * @param {File} file.
 * @param {Node} previewWrap - the image's preview "box". It is passed to this function
 * only because it needs to be passed to yet another function called here.
 */
export const sendFile = (file, path, post_id) => {
  var formData = new FormData();
  formData.append('image', file);
  formData.append('post_id', String(post_id));

  return $.ajax({
      type: 'POST',
      url: path,
      processData: false, // Don't try to process data.
      contentType: false, // Don't try to be smart about content-type.
      dataType: 'json',
      data: formData
      // Response handled with promises.
  });
};


/**
 * Fetches images for a given post.
 *
 * This is a GET request. Therefore, path should contain the post id,
 * something like /posts/3/images/fetch
 */
export const fetchPostImages = (path) => {
  return $.ajax({
    type: 'GET',
    url: path,
    dataType: 'json',
  });
};


export const destroy = (imageId) => {
  const path = `/admin/posts/${imageId}/images`;
  return $.ajax({
    type: 'DELETE',
    url: path,
    dataType: 'json'
  });
};


export const repositionDb = (data) => {
  addOverlay();
  return $.ajax({
      type: 'PATCH',
      url: `${location.href}/${data.imageId}/reposition`,
      data: data,
      dataType: 'json'
  });
};


/**
 * Sends crop data and returns new image attrs (mainly changes the version).
 *
 * @param imgAttrs ImageAttrs
 * @param cropData CropBoxData
 */
export const crop = (imgAttrs, cropData) => {
  addOverlay();
  return $.ajax({
    type: 'PATCH',
    url: `${location.href}/${imgAttrs.id}/crop`,
    data: {
      imgAttrs,
      cropData,
    },
    dataType: 'json'
  });
};
