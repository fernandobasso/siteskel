import * as $ from 'jquery';
import 'jquery-ui/ui/widgets/sortable.js';

import {
  addOverlay,
  removeOverlay,
 } from '../../shared/Helpers';

import {
  loadPreviews,
  addPreviewToDOM,
} from './ImagePreview';

import {
  sendFile,
 } from  './ImageRequests';

import {
  handleDrag,
  handleDestroy,
} from './GallerySort';

import {
  handleBtnCropOpen,
  handleBtnCropClose,
  handleBtnCropExecute,
} from './ImageCrop';

window.$ = $;

const $img = $('#img');
const $previewListWrp = $('.js-preview-list-wrp').first();

$img.on('change', function (evt) {
  let $form = $(this).closest('form');
  let path = $form.attr('actimn');
  let post_id = Number($form.find("[name='post_id']").val());

  addOverlay();

  const numFiles = this.files.length;
  let numProcessedFiles = 0;

  [].forEach.call(this.files, function (currentFile, idx) {
    sendFile(currentFile, path, post_id)
      .then((imageData) => {
        addPreviewToDOM(imageData, $previewListWrp);

        numProcessedFiles += 1;
        if (numProcessedFiles === numFiles) {
            removeOverlay();
        }
      });
  });
});


// Add existing images for this post to the page.
window.addEventListener('load', () => {
  // Only fetch images for this post if on /posts/<id>/images view.
  if (!/posts\/\d+\/images/.test(location.href)) {
      console.info('Not fetching images because we are not on images page for post.');
      return;
  }

  loadPreviews($previewListWrp);
});



// Listens and handles dragging/reordering and removing images.
// NOTE: jquery ui sortable adds stuff to .data() on each box causing
// problems with other things (like ajax sending ImageAttrs data to the server).
// const $gallerySort = new GallerySort($previewListWrp);
handleDrag();
handleDestroy();

handleBtnCropOpen();
handleBtnCropClose();
handleBtnCropExecute();
