/**
 * Return the full path to the image, with extension and even version to avoid cache.
 *
 * @param img ImageAttrs
 * @param size string - 'sm', 'md', 'lg' or 'orig'.
 * @return string
 */
export const imagePath = (img, size = 'md') => {
  let path = `/uploads/images/${img.post_id}-${img.id}-${size}.${img.ext}?v=${img.ver}`;
  return path;
};
