import  Cropper from 'cropperjs';
import * as $ from 'jquery';

import {
  removeOverlay,
} from '../../shared/Helpers';

import {
  imagePath,
 } from './ImageHelper';

import {
  crop,
} from './ImageRequests';

const $previewListWrp = $('.js-preview-list-wrp');
const $cropperWrp = $('.js-cropper-wrp');
const $btnCropExecute = $cropperWrp.find('.btn-crop-execute');
const $btnCropClose = $cropperWrp.find('.btn-crop-close');
let $currentImageBox;
let cropperjs;


/**
 * Listens to users clicking on the 'crop' button.
 */
export const handleBtnCropOpen = () => {
  $previewListWrp.on('click', '.btn-crop-open', (evt) => {
    $currentImageBox = $(evt.target).closest('.js-preview-wrp');
    displayCropper($currentImageBox.data('imgAttrs'));
  });
}


/**
 * Displays the image cropper ui to the user.
 *
 * @param attrs ImageAttrs
 */
const displayCropper = (attrs) => {
  if (!$cropperWrp || !$cropperWrp.find('img')) return;

  let $img = $cropperWrp.find('img');
  $img.attr('src', imagePath(attrs, 'orig'));
  $cropperWrp.removeClass('d-none');
  $cropperWrp.addClass('d-flex');

  cropperjs = new Cropper($img[0], {
    aspectRatio: NaN, // 16 / 16
    viewMode: 2,
    autoCropArea: 1.0,
  });
};


/**
 * Closes/cancels, stops displaying the cropper stuff.
 */
const cropClose = () => {
  if (!$cropperWrp) return;
  $cropperWrp.removeClass('d-flex').addClass('d-none');
  cropperjs.destroy();
};

export const handleBtnCropClose = () => {
  $btnCropClose.on('click', () => {
    cropClose();
  });
};

export const handleBtnCropExecute = () => {
  $btnCropExecute.on('click', (evt) => {
    console.log($currentImageBox.data('img-attrs'));
    crop(
      $currentImageBox.data('img-attrs'),
      cropperjs.getData(true),
    ).then((res) => {
      $currentImageBox.data('img-attrs', res.image);
      $currentImageBox.find('img.preview').attr('src', imagePath(res.image, 'sm'));
    })
    .always(() => {
      removeOverlay();
      cropClose();
    });
  });
};
