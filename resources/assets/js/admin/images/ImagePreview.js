import * as $ from 'jquery';

import {
  addOverlay,
  removeOverlay,
 } from '../../shared/Helpers';

import {
  fetchPostImages,
 } from  './ImageRequests';

/**
 * Creates a preview box without information, just the structure.
 *
 * @param image Object
 * @return Element
 */
const createPreview = (image) => {
  let tmpl = document.querySelector('#x-tmpl');

  if (!tmpl) {
    throw new TypeError('tmpl is null');
  }

  let tmp = document.createElement('div');
  tmp.innerHTML = tmpl.innerHTML.replace(/ +/, '');

  let preview = tmp.children[0];

  if (preview.querySelector('img')) {
    const src = `/uploads/images/${image.post_id}-${image.id}-sm.${image.ext}?v=${image.ver}`;
    preview.querySelector('img').src = src;
  }

  return preview;
}

export const readFile = (file) => {
  let reader = new FileReader();
  return new Promise((res, rej) => {
    let reader = new FileReader();
    reader.addEventListener('load', () => res(reader));
    reader.readAsDataURL(file);
  });
};

export const loadPreviews = ($previewListWrp) => {
  const postId = Number($("form [name='post_id']").val());

  if (!postId) {
    console.info('Not fetching images because we are not on images page for post.');
    return;
  }

  addOverlay();

  const path = `/admin/posts/${postId}/images/fetch`;

  // Passes the response to addPreviewsToDOM
  fetchPostImages(path).then(res => addPreviewsToDOM($previewListWrp, res)).always(removeOverlay);
};

export const addPreviewsToDOM = ($previewListWrp, response) => {
  $previewListWrp.empty();
  [].forEach.call(response, (item, idx) => {
    addPreviewToDOM(item, $previewListWrp);
  });
};

export const addPreviewToDOM = (attrs, $wrp) => {
  let img = Object.assign({}, attrs);
  let previewNode = createPreview(img);
  $(previewNode).data('img-attrs', attrs);
  $wrp.append(previewNode);
};
