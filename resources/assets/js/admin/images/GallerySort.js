import * as $ from 'jquery';
import 'jquery-ui';

import {
  removeOverlay,
 } from '../../shared/Helpers';

import {
  repositionDb,
  destroy,
} from './ImageRequests';

import {
  loadPreviews,
} from './ImagePreview';

const $listWrap = $('.js-preview-list-wrp').first();

const updatePositionData = ({ oldpos, newpos, $draggedItem }) => {
  if (!oldpos || !newpos || !$draggedItem) {
    return;
  }

  $draggedItem.data('img-attrs')['pos'] = newpos;

  // Fat arrow function messes with our `this` in this case.
  // Let's use plain good old function thing.
  $listWrap.children('.js-preview-wrp').each(function (index) {
    let pos = $(this).data('img-attrs')['pos'];
    let idx = index + 1;

    // We checked this above, so we can safely BANG ! the properties.
    if (newpos < oldpos) {
      if (idx > newpos && idx <= oldpos) {
        $(this).data('img-attrs')['pos'] = pos + 1;
      }
    }
    else if (newpos > oldpos) {
      if (idx >= oldpos && idx < newpos) {
        $(this).data('img-attrs')['pos'] = pos - 1;
      }
    }
  });
};


export const handleDrag = () => {
  let oldpos = null;
  let newpos = null;
  let imageId = null;
  let $draggedItem = null;

  $listWrap.sortable({
    items: '.js-preview-wrp', // Make the preview boxes draggable, but...
    handle: '.position',      // ...only drag if event is on .handle.
    // Dragging starts.
    start: (evt, $ui) => {
      oldpos = $ui.item.data('img-attrs')['pos'];
      console.log('start oldpos', oldpos)
    },

    // Dragging ends.
    update: (evt, $ui) => {
      // index() + 1 because index is zero-based, but our DB thing starts with 1, not 0.
      newpos = $ui.item.index() + 1;
      imageId = $ui.item.data('img-attrs')['id'];
      $draggedItem = $ui.item;

      repositionDb({ imageId, oldpos, newpos })
        .then(res => {
          removeOverlay();
          if ('success' !== res.status) {
            console.warn('Error reordering images on DB.');
            return;
          }

          updatePositionData({ imageId, oldpos, newpos, $draggedItem });
        });
    }

  });
};


export const handleDestroy = () => {
  $listWrap.on('click', '.btn-action.remove', (evt) => {
    let $box = $(evt.target).closest('.js-preview-wrp');

    destroy($box.data('img-attrs').id)
      .then((res) => {
        if (res.status !== 'success') {
          alert(res.msg);
          return;
        }

        $box.remove();
      })
      .always(() => {
        removeOverlay();

        // So we bring the new image positions from the server
        // until we implement some logic on the UI to avoid this.
        loadPreviews($listWrap);
      });
  });
};

