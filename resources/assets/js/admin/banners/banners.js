import * as $ from 'jquery';

const BANNER_WIDTH = parseInt(document.querySelector('input[name=banner-width]').value, 10);
const BANNER_HEIGHT = parseInt(document.querySelector('input[name=banner-height]').value, 10);

const readFile = (file) => {
  let reader = new FileReader();
  return new Promise((resolve, reject) => {
    let reader = new FileReader();
    reader.addEventListener('load', () => resolve(reader));
    reader.readAsDataURL(file);
  });
};

/**
 * Return false if banner dimensions are OK. A string if
 * something is wrong.
 */
const validateBannerDimensions = ({ target: { naturalWidth, naturalHeight } }) => {
  const $elem = $('.js-banner-dimensions-validation');
  const isValid = naturalWidth === BANNER_WIDTH && naturalHeight === BANNER_HEIGHT;
  if (!isValid) {
    $elem.text(`Dimensões inválidas: ${naturalWidth}x${naturalHeight}`);
    $elem.removeClass('d-none');
  }
  else {
      $elem.addClass('d-none');
  }
};

const $fileInput = $('#banner');
const $fileUploadPreview = $('.banner-upload-preview');

$fileInput.on('change', (evt) => {
  const input = evt.target;
  const file = input.files[0];

  if (!file) {
    console.error('File not found on #banner');
  }

  const img = document.createElement('img');
  img.setAttribute('class', 'banner-img');
  img.file = file;

  readFile(file).then(({ result }) => {
    img.src = result;
    $fileUploadPreview.addClass('show');
  });

  $fileUploadPreview.html('');
  $fileUploadPreview.append(img);

  img.addEventListener('load', validateBannerDimensions);
});

$('.js-banner-index-wrp').each(function () {
  const $self = $(this);
  const bannerId = $self.data('banner-id');
  const $btnDestroy = $self.find('.js-btn-destroy');
  const $btnDestroyConfirm = $self.find('.js-btn-destroy-confirm');
  const $btnDestroyCancel = $self.find('.js-btn-destroy-cancel');
  const $wrpBtnsDestroy = $self.find('.js-wrp-btns-destroy');

  $btnDestroy.on('click', (evt) => {
    evt.preventDefault();
    $btnDestroy.removeClass('show');
    $btnDestroy.addClass('hide');
    $wrpBtnsDestroy.removeClass('hide');
    $wrpBtnsDestroy.addClass('show');
  });

  $btnDestroyCancel.on('click', (evt) => {
    evt.preventDefault();
    $btnDestroy.removeClass('hide');
    $btnDestroy.addClass('show');
    $wrpBtnsDestroy.removeClass('show');
    $wrpBtnsDestroy.addClass('hide');
  });

  $btnDestroyConfirm.on('click', function (evt) {
    evt.preventDefault();

    const route = $(this).data('route');

    $.ajax({
      url: route,
      type: 'DELETE',
      headers: { 'X-CSRF-TOKEN': $('meta[name=csrf-token').attr('content') },
      success: function (res) {
        $self.fadeOut('slow', function () {
          $(this).remove();
        });
      },
    });
  });
});

