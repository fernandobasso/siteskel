import * as $ from 'jquery';

$(window).scroll(function() {
  // Only show in larger screens.
  if ($(document).width() < 960) {
    return;
  }
  if ($(this).scrollTop() >= 200) {
    $('#back-to-top').fadeIn(200);
  } else {
    $('#back-to-top').fadeOut(200);
  }
});

$("#back-to-top").click(function () {
    //1 second of animation time
    //html works for FFX but not Chrome
    //body works for Chrome but not FFX
    //This strange selector seems to work universally
    $("html, body").animate({scrollTop: 0}, 1000);
});
