import * as $ from 'jquery';

import '../../images/shared/spinner.gif';

/**
 * Checks whether we are on the home page.
 *
 * @impure: relies on external SimpleCMS.routeName and passing
 *          it as arg would just make it more verbose to use.
 *
 * @return boolean
 */
export const isHomePage = () => SimpleCMS.routeName === 'site-home';


/**
 * Checks whether the current page has an element whose id matches location.hash.
 *
 * @param string toId the id of the element to scroll to.
 *
 * @return boolean
 */
const canScrollOnThisPage = (toId) => {
  if (!toId || !/#\w+/.test(toId)) {
      return false;
  }

  // If "this page" has an element with the id toId, then we can scroll to it.
  return null !== document.querySelector(toId);
}


export const addOverlay = () => {
  const img = '/images/spinner.gif';
  $(document.body).append(`<div id='overlay1'><img src='${img}'></div>`);
};

export const removeOverlay = () => {
  $('#overlay1').fadeOut( 800, function () {
      $(this).remove();
  });
};


/**
 * Scrools the browser to the element with the id `toId`.
 *
 * @param toId string - the id/hash to scroll to. Examples:
 *                      '#foo', '#row-contact', '#featured'.
 *                      Note the string includes the '#' character so that
 *                      the jQuery selector works. Just get it with `location.hash`
 *                      and be done with it.
 */
const scrollToId = (toId) => {

  // If location.hash is empty we have nothing to scroll too.
  // This function should only be called when we know there is
  // the hash fragment, but it won't hurt being defensive here.
  if ('' === toId) {
      return;
  }

  $('#navbar-top .nav-link').removeClass('active');

  $('#navbar-top .nav-link[data-scroll-to="' + toId + '"]').addClass('active');

  console.log('scrollToId', $(toId).offset());

  const intervalId = setInterval(() => {
    //
    // Sometimes, $(toId).offset() is still undefined because the DOM is not ready yet.
    // This workaround should prevent that problem.
    //
    if (document.readyState === 'interactive' || document.readyState === 'complete') {
      $('html, body').animate({
        // `toId` must start with a `#` char.
        scrollTop: $(toId).offset().top
      }, 700, 'swing', () => {
        location.hash = toId;
      });

      clearInterval(intervalId);
    }
  }, 300);
}


export const handleClickScrollToId = () => {
  $('.scroll-to').click(evt => {
    let toId = $(evt.currentTarget).data('scroll-to');

    if (!toId) return;

    // On the home page, we don't want to follow the links,
    // but just to scroll instead. Also, if the hash fragment
    // matches an element on the page, then do not follow the
    // link and allow a smooth scroll.
    if (canScrollOnThisPage(toId)) {
      $('#navbar-top .nav-link').removeClass('active');
      $(evt.target).addClass('active');
      evt.preventDefault();
    }

    scrollToId(toId);
  });
}

/**
 * Decides whether we have some hint to scroll the page.
 *
 * If we are not on the home page and do not have a #hash on
 * the url, then we have nothing to scroll to. But if we _are_
 * on the home page and have a #hash fragment, then we return true.
 *
 * @impure: relies on environment location.hash and passing it as
 *          argument just to make it pure would become cumbersome.
 *
 * @return boolean
 */
const shouldScroll = () => isHomePage() && location.hash !== '';

/**
 * If we can scroll, do it!
 *
 * @impure: relies on environment location.hash and passing it as
 *          argument just to make it pure would become cumbersome.
 */
export const attemptScroll = () => {
  if (shouldScroll()) {
    setTimeout(() => {
      scrollToId(location.hash);
    }, 100)
  }
};


/**
 * Prevents automatic browser navigation to location.hash.
 *
 * @impure: causes side effect of preventing browser scroll.
 *
 * https://stackoverflow.com/questions/3659072/how-to-disable-anchor-jump-when-loading-a-page
 *
 * @param callableScrollFn Function - the function to do the smooth scroll.
 */
export const preventDefaultScroll = (callableScrollFn) => {
  if (location.hash === '') return;

  setTimeout(() => {
    window.scrollTo(0, 0);
    callableScrollFn();
  }, 1);
}

/**
 * Pops up a message box on the screen.
 *
 * @impure: modifies the DOM.
 *
 * @param msg string
 * @param type string - the css class like 'success', 'warning', 'info', 'danger'
 * @param ttl number - time to live, default 2 seconds (2k milliseconds)
 */
export const flashMsg = (msg, type, ttl = 20000) => {
  let html = `
    <div class="flash-msg flash-msg-${type} text-center" id="flash-msg" style="display: none;">
      <div class="msg">
        ${msg}
      </div>
      <button type="button" id="btn-close-x" class="close btn-close-x" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  `;

  $(document.body).append(html);

  let $flashMsg = $('#flash-msg');

  $flashMsg.fadeIn('slow', function () {
    setTimeout(() => {
      $(this).fadeOut('slow', () => {
        $(this).remove();
      });
    }, ttl);
  });
};


/**
 * Listens for click on x btn that removes a flash msg.
 */
export const listenClickRemoveFlashMsg = () => {
  $(document.body).on('click', '#btn-close-x', function () {
    $(this).closest('#flash-msg').fadeOut('slow', function () {
      $(this).remove();
    });
  });
};

export const countWords = str => str.trim().split(/\s+/).length;

export const cutPlainText = (text, len, trail = '...') => {
  if (text.length > len) {
    return `${text.substr(0, len)}${trail}`;
  }
  return `${text}`;
};
