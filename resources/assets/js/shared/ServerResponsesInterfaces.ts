enum ResponseStatus {
    // Not these are the same names as bootstrap classes so
    // they can be convenientely used with some javascript
    // flash messages and information boxes.
    success,
    danger,
    warning,
    info
};

interface ServerResponse {
    status: ResponseStatus;
    msg: string;
}


