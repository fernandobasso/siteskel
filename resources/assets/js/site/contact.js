import * as $ from 'jquery';

import {
  addOverlay,
  removeOverlay,
  flashMsg,
} from '../shared/Helpers';

const $form = $('#contactform1');

/**
 * Serializes and sends data to the server. Assumes the form is filled in.
 *
 * @param  $form JQuery - a jquery form object.
 */
const postData = ($form) => {
  let data = $form.serialize();

  return $.ajax({
    type: 'POST',
    url: `${SimpleCMS.baseurl}/contact`,
    dataType: 'json',
    data: data
  });
};

export const handleContactFormSubmit = () => {
  if ($form.length !== 1) return;

  $form.on('submit', (evt) => {
    evt.preventDefault();
    addOverlay();
    postData($form)
      .then((res) => {
        flashMsg(res['msg'], res['status'], 35000);
        $form[0].reset();
      })
      .always(removeOverlay);
  });
};

