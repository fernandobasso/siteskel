
import * as jQuery from 'jquery';

window.jQuery = jQuery;
window.$ = jQuery;

import {
  handleContactFormSubmit,
} from './contact';

import '../../../../node_modules/bootstrap/dist/js/bootstrap.min.js';

import {
  preventDefaultScroll,
  attemptScroll,
  handleClickScrollToId,
  listenClickRemoveFlashMsg,
} from '../shared/Helpers';

import '../shared/scroll-to-top';

import '../../images/shared/sprite-vbsmidia-logo-squares.png';

preventDefaultScroll(attemptScroll);

listenClickRemoveFlashMsg();

handleClickScrollToId();

handleContactFormSubmit();
