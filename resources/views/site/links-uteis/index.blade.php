@extends('site.site-main-layout')

@section('page_title', 'Links Úteis')

@section('content-body')

<section class="links-uteis links-uteis-index">

    <div class="container content-body">

        <h2 class="mb-3">Links Úteis</h2>

        <div class="row">
          @foreach ($links_uteis AS $link_util)
            <div class="col-12 mb-4">
              <div class="card">
                <div class="card-header bg-primary text-white text-uppercase text-center">
                  {{ $link_util->title }}
                </div>
                <div class="card-body">
                  <div class="card-text">
                    {!! $link_util->text !!}
                  </div>
                </div>
              </div>
            </div>
          @endforeach
        </div>

    </div>

</section>

@endsection


