<section class="row-contato" id="row-contato">

    <div class="container form-wrp mx-auto">

        <h2 class="text-center mb-4">Fale Conosco</h2>

        <form id="contactform1" method="POST">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="contact-name">Nome</label>
                <input type="text" class="form-control" id="contact-name"
                    name="contact[name]" placeholder="Seu nome"
                    pattern=".{2,}" required>
            </div>

            <div class="form-group">
                <label for="contact-email">E-mail</label>
                <input type="email" class="form-control" id="contact-email"
                    name="contact[email]" placeholder="Seu email"
                    required>
            </div>

            <div class="form-group">
                <label for="contact-phone">Telefone</label>
                <input type="text" class="form-control" id="contact-phone"
                    name="contact[phone]" placeholder="Telefone"
                    required>
            </div>

            <div class="form-group">
                <label for="contact-message">Mensagem</label>
                <textarea class="form-control" id="contact-message"
                    name="contact[message]" placeholder="Escreva aqui a sua mensagem"
                    rows="5" required></textarea>
            </div>

            <input class="btn btn-primary" type="submit" value="Enviar">
        </form>


        {{--
            Rua Bento Gonçalves, 114
            Passo Fundo

            Call (54) 3313-5107
        --}}
    </div>

</section>
