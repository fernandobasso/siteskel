<div class="navbar-top">
    <nav class="navbar navbar-expand-lg">

        {{--
        Toggle button is on topbar.blade.php
        --}}
        <div class="collapse navbar-collapse" id="navbar-top">

            <ul class="navbar-nav justify-content-center py-2">
                <li class="nav-item">
                    <a class="anim nav-link rounded {{ link_active('/') }}"
                      href="{{ url('/') }}">
                      Início
                    </a>
                </li>
                <li class="nav-item">
                    <a class="anim nav-link rounded {{ link_active('servicos') }} scroll-to"
                      data-scroll-to="#row-servicos-home"
                      href="{{ url('/#row-servicos-home') }}">
                      Serviços
                    </a>
                </li>
                <li class="nav-item">
                    <a class="anim nav-link rounded {{ link_active('sobre') }} scroll-to"
                      data-scroll-to="#row-sobre"
                      href="{{ url('/#row-sobre') }}">
                      Sobre
                    </a>
                </li>
                <li class="nav-item">
                    <a class="anim nav-link rounded {{ link_active('novidades') }} scroll-to"
                      data-scroll-to="#row-news-home"
                      href="{{ url('/#row-news-home') }}">
                      Novidades
                    </a>
                </li>
                <li class="nav-item">
                    <a class="anim nav-link rounded scroll-to"
                        data-scroll-to="#row-localizacao"
                        href="{{ url('/#row-localizacao') }}">
                        Localização
                    </a>
                </li>
                <li class="nav-item">
                    <a class="anim nav-link rounded scroll-to"
                        data-scroll-to="#row-contato"
                        href="{{ url('/#row-contato') }}">
                        Contato
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>
