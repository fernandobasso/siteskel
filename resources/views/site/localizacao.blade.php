<section class="row-localizacao py-4" id="row-localizacao">

  <div class="container">

    <h2 class="text-center mb-4">Localização</h2>

    <div class="row">

      <div class="col-12">
        <p class="text-center">
            Rua TODO 0000, Loja 00, CEP 00000-000, Centro, Passo Fundo RS
        </p>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d47279.49458117557!2d-52.436683453639596!3d-28.26591059673981!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94e2bf7e7e8ce7ab%3A0xb328960779f49b4e!2sPasso+Fundo+-+RS!5e0!3m2!1sen!2sbr!4v1548800809498" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>

    </div>
  </div>

</section>
