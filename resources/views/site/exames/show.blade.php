@extends('site.site-main-layout')

@section('page_title', $exame->title)

@section('content-body')

<section class="exames exames-show">

    <div class="container content-body">

        <section class="exames exames-show clearfix">
            <div class="exames exames-intro">
                <h3 class="text-center">{{ $exame->title }}</h3>
                @if (isset($exame->images[0]))
                    <div class="wrp-img mb-4" >
                        <div class="d-flex">
                            <a class="mx-auto"
                              href="{{ asset($exame->images[0]->path('orig')) }}"
                                data-lightbox="gallery-clinicapulmao">
                                <img class="img-fluid img-thumbnail"
                                src="{{ asset($exame->images[0]->path('md')) }}" alt="{{ $exame->title }}">
                            </a>
                        </div>
                    </div>
                @endif
                <p>{!! $exame->intro !!}</p>
            </div>
            <div class="exames exames-text">{!! $exame->text !!}</div>

            <div class="wrp-gallery row">
                <style>
                    .wrp-gallery .d-flex {
                        align-items: center;
                    }
                </style>
                @foreach ($exame->images AS $img)
                    <div class="box col-lg-3 col-md-4 col-xs-6 mb-4">
                        <a class="d-flex"
                            href="{{ asset($img->path('orig')) }}"
                            data-lightbox="gallery-appg">
                            <img class="img-fluid"
                                src="{{ asset($img->path('md')) }}" alt="default alt">
                        </a>
                    </div>
                @endforeach
            </div>
        </section>

        <div class="text-center text-md-right mt-5">
            <a class="see-more see-more-exames"
                href="{{ route('site.exames.index') }}"
                >Conheçer outros exames
                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </a>
        </div>

    </div>

</section>

@endsection
