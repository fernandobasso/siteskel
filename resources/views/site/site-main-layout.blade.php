<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/shared/favicon.png?v=1') }}">
    {{--
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/fa47/font-awesome.css') !!}" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    --}}

    <link
      rel="stylesheet"
      type="text/css"
      href="{!! asset("assets/css/site/bootstrap-custom-site.css?") !!}"
    />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">



    @if (\Request::route()->getName() == 'site-home')
      <link
        rel="stylesheet"
        type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
      <link
        rel="stylesheet"
        type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">
    @endif

    @if (
      in_array(\Request::route()->getName(), [
        'site.posts.show',
        'site.sobre.show',
        'site.servicos.show'
      ]))
      <link
        rel="stylesheet"
        type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/css/lightbox.min.css">
    @endif

    <link rel="stylesheet" type="text/css" href="{!! asset('assets/css/site/main-site.css?') !!}" />

    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>
      @if (View::hasSection('page_title'))
        @yield('page_title') |
      @endif
      {{ env('APP_NAME') }} - {{ env('APP_PURPOSE') }}
    </title>
    @include('site.incjs')
  </head>
  <body>
    @include('site.topbar')

    @include('site.navbar-top')

    @if (\Request::route()->getName() == 'site-home')
      @include('site.home.home')
    @endif

    {{-- For internal pages. --}}
    @if (\Request::route()->getName() != 'site-home')
      @yield('content-body')
    @endif

    @include('site.localizacao')

    @include('site.form-contato')

    <footer class="main-footer" id="main-footer">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-12 col-sm-5">
            <p class="mb-0 text-center">{{ env('APP_NAME') }}</p>
          </div>
          <div class="wrp-facebook col-12 col-sm-2 text-center">
            <a
              class="d-block ml-0 my-3 my-sm-0 ml-sm-5"
              href="https://www.facebook.com/integradose/" target="_blank">
              <img src="{{ asset('/images/facebook-32x32.png') }}" alt="icon facebook">
            </a>
          </div>
          <div class="col-12 col-sm-5 d-md-flex">
            <div class="ml-md-auto text-center">
              <div class="developer">
                <a href="http://www.vbsmidia.com.br" target="_blank">
                  <span>Desenvolvido por</span> <span class="sprite"></span> <span>VBS Mídia</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <div class="back-to-top" id="back-to-top">
      voltar ao topo
      <i class="far fa-arrow-alt-circle-up"></i>
    </div>

    <script
      type="text/javascript"
      src="{{ mix("assets/js/site/main-site.js") }}"
    ></script>

    {{-- Do something specific for home page. --}}
    @if ('site-home' == \Request::route()->getName())
      <script
        type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js">
      </script>

      <script type="text/javascript">
        $('.js-banners-home').slick({
            autoplay: true,
            autoplaySpeed: 4000,
            arrows: false,
            dots: true,
            infinite: true,
            speed: 700,
            slidesToShow: 1,
            adaptiveHeight: true
        })

        // 480px is bootstrap 4 xs breakpoint for our custom bootstrap setup.
        if (document.body.getBoundingClientRect()['width'] < 480) {
            $('.slick-next').remove();
            $('.slick-prev').remove();
        }
      </script>
    @endif

    @if (
      in_array(\Request::route()->getName(), [
        'site.posts.show',
        'site.sobre.show',
        'site.servicos.show',
      ]))
      <script
        src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.9.0/js/lightbox.min.js"
        type="text/javascript"
      ></script>
    @endif

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-19539500-28', 'clinicadopulmao.med.br');
      ga('send', 'pageview');

    </script>
</body>
</html>
