@extends('site.site-main-layout')

@section('page_title', $sobre->title)

@section('content-body')

<section class="sobre sobre-show">

    <div class="container content-body">

        <section class="sobre sobre-show clearfix">
            <div class="sobre sobre-intro">
                <h3 class="text-center">{{ $sobre->title }}</h3>
                @if (isset($sobre->images[0]))
                    <div class="wrp-img">
                        <div class="d-flex justify-content-center">
                            <a
                                class="mb-4"
                                href="{{ asset($sobre->images[0]->path('orig')) }}"
                                data-lightbox="gallery-condarpf">
                                <img
                                  class="img-fluid img-thumbnail"
                                  src="{{ asset($sobre->images[0]->path('md')) }}"
                                  alt="{{ $sobre->title }}">
                            </a>
                        </div>
                    </div>
                @endif
                <p>{!! $sobre->intro !!}</p>
            </div>
            <div class="sobre sobre-text">{!! $sobre->text !!}</div>

            <div class="wrp-gallery row">
                <style>
                    .wrp-gallery .d-flex {
                        align-items: center;
                    }
                </style>
                @foreach ($sobre->images AS $img)
                    <div class="box col-lg-3 col-md-4 col-xs-6 mb-4">
                        <a class="d-flex"
                            href="{{ asset($img->path('orig')) }}"
                            data-lightbox="gallery-appg">
                            <img class="img-fluid"
                                src="{{ asset($img->path('md')) }}" alt="default alt">
                        </a>
                    </div>
                @endforeach
            </div>
        </section>

    </div>

</section>

@endsection

