<div class="topbar py-2">

  {{--
    We have to use class .navbar-light because toggle button and icon styles
    are applied if they are descendant of .navbar-light. We also had to use
    some custom css to hide the toggler from lg and up.
  --}}
  <div class="navbar-light container">

    <div class="row justify-content-between">

      <div class="col-12 col-md-6 d-block text-center d-md-flex text-lg-left">
        <a class="navbar-brand" href="{{ url('/') }}">
          <img
            class="logo img-fluid"
            src="{{ asset('assets/images/logo.png?v=1') }}"
            alt="TODO: Logo" />
        </a>
      </div>

      <div
         class='phone-wrp col-12 col-md-5 col-lg-6 mt-2 mt-md-0 d-block d-md-flex
        align-items-center text-left text-sm-center text-lg-right justify-content-end'>
        <div class="wrp text-dark">
          <div>
            <i class="text-primary fa fa-phone"></i>
            (00) 0000.0000 | 0 0000.0000
          </div>
          <div>
            <i class="text-primary fa fa-envelope"></i>
            contato@siteskeltempl.com.br
          </div>
        </div>
      </div>

      <button class="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbar-top"
        aria-controls="navbar-top"
        aria-expanded="false"
        aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

    </div> {{-- .row --}}

  </div>

</div>
