<div class="home-content">

    @include('site.home.banners')

    @include('site.home.services')

    @include('site.home.about')

    @include('site.home.news')

</div>
