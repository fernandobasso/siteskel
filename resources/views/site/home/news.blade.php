<div class="row-news-home news-home-index py-5" id="row-news-home">

  <div class="container">

      <h2 class="text-center mb-4">Novidades</h2>

      @foreach ($news AS $new)
        <?php
        $post_url = route('site.posts.show', ['post_id' => $new->id, 'title_slug' => str_slug($new->title)]);
        $post_intro = $new->intro; //str_limit($new->intro, 220, ' (...)');
        ?>
          <div class="news-item row mb-4">
            <h5 class="mt-0 news-item-title d-block d-sm-none my-3 mx-3">{{ $new->title }}</h5>
            <a class="news-item-img-link col-12 col-md-5 mb-3 p-4"
              href="{{ $post_url }}"
              @if (isset($new->images[0]))
              style="background-image: url({{ img_post($new->images[0]) }})"
              @endif
            >
            </a>
            <div class="news-item-text col-12 col-md-7">
              <h5 class="mt-0 news-item-title d-none d-sm-block my-3">{{ $new->title }}</h5>
              <div class="intro news-item-intro">{{ $post_intro }}</div>
              <a class="link-read-more anim text-center" href="{{ $post_url }}">
                <span>Mais ›</span>
                <i class="fas fa-long-arrow-alt-right"></i>
              </a>
            </div>
        </div>
      @endforeach


      <div class="text-center mt-5">
        <a class="see-more see-more-news"
          href="{{ route('site.news.index') }}"
        >
          Ver mais novidades
          <i class="fas fa-angle-double-right" aria-hidden="true"></i>
        </a>
      </div>
  </div>

</div>
