<section class="row-sobre py-5" id="row-sobre">

    <div class="container">

        <h2 class="text-center mb-4">{{ $about->title }}</h2>

        <p>{{ $about->intro }}</p>

        {!! $about->text !!}

    </div>

</section>
