<div class="banners pt-5 pb-4">
  <div class="container">
    <div class="col-12 js-banners-home px-0 px-sm-2">
      @foreach ($banners as $banner)
        <a
          class="scroll-to"
          {!! $banner->href() !!}
        >
          <img
            class="img-fluid"
            src="{{ $banner->path() }}"
            alt="{{ $banner->title }}"
          />
        </a>
      @endforeach
    </div>
  </div>
</div>
