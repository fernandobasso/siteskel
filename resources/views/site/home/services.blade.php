<div class="row-services-home services-home-index pt-4 pb-5" id="row-services-home">

  <div class="container">

    <h2 class="text-center mb-4">Serviços</h2>

    <div class="row services">
      @php $index = 0; @endphp
      @foreach ($services AS $service)

        <a href="{{ url("/services/{$service->id}/" . str_slug($service->title)) }}"
            class="service idx-{{ $index}} col-12 col-md-6 p-4 mb-4 mb-md-0">
            <div
              class="bg d-none d-md-block"
              @if (count($service->images) > 0)
              style="background-image: url({{ img_post($service->images[0]) }})"
              @endif
              ></div>
            <h2 class="title text-center px-4">{{ $service->title }}</h2>
            <div class="text text-center px-4">
              {{ str_limit($service->intro, 64) }}
            </div>
            <div class="d-none d-lg-block">
              <i class="fas fa-arrow-circle-right"></i>
            </div>
            @if (count($service->images) > 0)
            <img
              class="d-block d-md-none"
              src="{{ img_post($service->images[0]) }})"
              alt="{{ $service->title }}">
            @endif
        </a>

        {{-- https://getbootstrap.com/docs/4.0/layout/grid/#equal-width-multi-row --}}
        @if (1 == $index)
          <div class="w-100 d-none d-md-block"></div>
        @endif

        @php $index -= -1; @endphp
      @endforeach
    </div>

  </div>{{-- div.container --}}

</div>
