@extends('site.site-main-layout')

@section('page_title', $servico->title)

@section('content-body')

  <section class="servicos servicos-show">

    <div class="container content-body">

      <section class="servicos servicos-show clearfix">
        <div class="servicos servicos-intro">
          <h3 class="mb-4">{{ $servico->title }}</h3>
          @if (isset($servico->images[0]))
            <img
              class="img-servico-show"
              src="{{ img_post($servico->images[0], 'lg') }}"
              alt="{{ $servico->title }}" />
          @endif
          <p class="mt-3">{!! $servico->intro !!}</p>
        </div>
        <div class="servicos servicos-text">
          {!! $servico->text !!}
        </div>

      </section>

      {{--
        <div class="text-center text-md-right mt-5">
            <a class="see-more see-more-servicos"
                href="{{ route('site.servicos.index') }}"
                >Conheçer outros servicos
                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </a>
        </div>
      --}}

    </div>

  </section>

@endsection
