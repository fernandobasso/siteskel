@extends('site.site-main-layout')

@section('page_title', 'Exames')

@section('content-body')

<section class="exames exames-index">

    <div class="container content-body">

        <h2 class="mb-3">Tratamentos</h2>

          @foreach ($exames AS $new)
            <?php
            $post_url = route('site.exames.show', ['post_id' => $new->id, 'title_slug' => str_slug($new->title)]);
            $post_intro = $new->intro;
            ?>
              <div class="exames-item row mb-4 wow fadeInLeftBig" data-wow-delay="0.5s">
                <h5 class="mt-0 exames-item-title d-block d-sm-none my-3 mx-3">{{ $new->title }}</h5>
                <a class="exames-item-img-link col-12 col-sm-3 mb-3"
                  href="{{ $post_url }}">
                  @if (isset($new->images[0]))
                  <img class="mx-auto img-fluid" src="{{ img_post($new->images[0]) }}"
                    alt="{{ $new->title }}">
                  @endif
                </a>
                <div class="exames-item-text col-12 col-sm-9">
                  <h5 class="mt-0 exames-item-title d-none d-sm-block my-3">{{ $new->title }}</h5>
                  <div class="intro exames-item-intro">{{ $post_intro }}</div>
                  <a class="link-read-more text-center" href="{{ $post_url }}">Continue lendo »</a>
                </div>
            </div>
          @endforeach

    </div>

</section>

@endsection
