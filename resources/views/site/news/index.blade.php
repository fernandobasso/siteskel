@extends('site.site-main-layout')

@section('page_title', 'Novidades')

@section('content-body')

<section class="news news-index">

    <div class="container content-body">

        <h2 class="mb-3">Notícias</h2>

          @foreach ($news AS $new)
            <?php
            $post_url = route('site.posts.show', ['post_id' => $new->id, 'title_slug' => str_slug($new->title)]);
            $post_intro = $new->intro;
            ?>
              <div class="news-item row mb-4 wow fadeInLeftBig" data-wow-delay="0.5s">
                <h5 class="mt-0 news-item-title d-block d-sm-none my-3 mx-3">{{ $new->title }}</h5>
                <a class="news-item-img-link col-12 col-sm-5 mb-3"
                  href="{{ $post_url }}">
                  @if (isset($new->images[0]))
                  <img class="mx-auto img-fluid" src="{{ img_post($new->images[0]) }}"
                    alt="{{ $new->title }}">
                  @endif
                </a>
                <div class="news-item-text col-12 col-sm-7">
                  <h5 class="mt-0 news-item-title d-none d-sm-block my-3">{{ $new->title }}</h5>
                  <div class="intro news-item-intro">{{ $post_intro }}</div>
                  <a class="link-read-more anim text-center" href="{{ $post_url }}">
                    <span>Mais ›</span>
                    <i class="fas fa-long-arrow-alt-right"></i>
                  </a>
                </div>
            </div>
          @endforeach

    </div>

</section>

@endsection

