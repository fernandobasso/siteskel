@extends('site.site-main-layout')

@section('page_title', $new->title)

@section('content-body')

<section class="news news-show">

    <div class="container content-body">

        <section class="news news-show clearfix">
            <div class="news news-intro">
                <h3 class="d-md-none">{{ $new->title }}</h3>
                @if (isset($new->images[0]))
                    <div class="wrp-img float-left mr-md-3 mb-3 mb-md-0" >
                        <div class="d-flex">
                            <a href="{{ asset($new->images[0]->path('orig')) }}"
                                data-lightbox="gallery-clinica-pulmao">
                                <img class="img-fluid img-thumbnail"
                                src="{{ asset($new->images[0]->path('md')) }}" alt="{{ $new->title }}">
                            </a>
                        </div>
                    </div>
                @endif
                <h3 class="d-none d-sm-none d-md-block">{{ $new->title }}</h3>
                <p>{!! $new->intro !!}</p>
            </div>
            <div class="news news-text">{!! $new->text !!}</div>

            <div class="wrp-gallery row">
                <style>
                    .wrp-gallery .d-flex {
                        align-items: center;
                    }
                </style>
                @foreach ($new->images AS $img)
                    <div class="box col-lg-3 col-md-4 col-xs-6 mb-4">
                        <a class="d-flex"
                            href="{{ asset($img->path('orig')) }}"
                            data-lightbox="gallery-appg">
                            <img class="img-fluid"
                                src="{{ asset($img->path('md')) }}" alt="default alt">
                        </a>
                    </div>
                @endforeach
            </div>
        </section>

        <div class="text-center text-md-right mt-5">
            <a class="see-more see-more-news"
                href="{{ route('site.news.index') }}"
                >Ver mais novidades
                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            </a>
        </div>

    </div>

</section>

@endsection
