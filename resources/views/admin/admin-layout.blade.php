<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{{ mix('assets/css/admin/bootstrap-custom-admin.css') }}" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="{{ mix('assets/css/admin/main-admin.css') }}" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/shared/favicon.png?v=0') }} ">
        <title>ADMIN - {{ env('APP_NAME') }} | by VBS Mídia</title>
        @include('admin.incjs')
    </head>
    <body>


        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">

                <a class="navbar-brand" href="#">ADMIN - {{ env('APP_NAME') }}</a>

                {{-- TODO: We should never be in admin without a user, but... --}}
                @if (auth()->user())
                    <div>
                        <i class="far fa-user"></i>
                        {{ auth()->user()->name }}
                        {{ auth()->user()->has_role }}
                        <form
                          class="text-right"
                          method="POST" action="{{ route('logout') }}">
                          <button
                            class="btn btn-outline-secondary"
                            type="submit">
                            <i class="fas fa-sign-out-alt"></i>
                            Sair
                            {{ csrf_field() }}
                        </form>
                    </div>
                @endif

            </div>
        </nav>

        <div class="container content-body">
            @section('sidebar')
                <section class="main-sidebar">
                    <ul class="nav flex-column">
                        @if (auth()->user()->isAdmin())
                            <li class="nav-item">
                                <a class="nav-link {{ link_active('admin/post-categories') }}"
                                    href="{{ route('post-categories.index') }}">Categorias de Publicações</a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link {{ link_active('admin/posts') }}"
                                href="{{ route('posts.index') }}">Publicações</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ link_active('admin/posts/create') }}"
                                href="{{ route('posts.create') }}">Publicar Conteúdo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ link_active('admin/banners') }}"
                                href="{{ route('admin.banners.index') }}">Banners</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ link_active('admin/videos') }}"
                                href="{{ route('videos.index') }}">Vídeos</a>
                        </li>
                    </ul>
                </section>
            @show

            <div class="container main-container">
                @yield('content')
            </div>
        </div> {{-- .content-body --}}

        <footer class="main-footer">
            <div class="container">
                <p class="mb-0 text-center">Copyright VBS Mídia {{ getCurrentYear() }}</p>
            </div>
        </footer>

        <script type="text/javascript" src="{{ mix('assets/js/admin/main-admin.js') }}"></script>

        @yield('page-scripts')
    </body>
</html>
