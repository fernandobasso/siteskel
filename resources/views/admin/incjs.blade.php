{{--
  Take a look at: resources/assets/ts/globals.d.ts
  If you add or change something in this global object, update
  `globals.d.ts` to reflect the change.
--}}
<script>
var SimpleCMS = {
    baseurl: '{{ url('/admin') }}',
    routeName: '{{ Route::currentRouteName() }}'
};
</script>

