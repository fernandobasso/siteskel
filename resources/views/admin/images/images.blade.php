@extends('admin.admin-layout')

@section('title', 'Imagens')

@section('content')

    @php
        $btns['cancel']['display'] = true;
    @endphp
    @include('admin.shared.crud-actions-header', [
        'title' => "Imagens do post <em>{$post->title}</em>",
        'btns' => $btns
    ])

    <div class="container mb-3">
        <div class="row imgs-form-wrp p-3">
            <div class="col-sm-5">
                <form action="{{ route('admin.images.store', ['post_id' => $post->id]) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="post_id" value="{{ $post->id }}">
                    <div id="selelect-imgs-wrp" class="select-imgs-wrp">
                        <label for="img" class="mb-0">
                            Selecione uma ou mais imagens
                            <i class="fa fa-upload"></i>
                        </label>
                        <input multiple name="img" id="img" type="file">
                    </div>
                </form>
            </div>
            <div class="col-sm-7 messages">
                <div id="images-messages"></div>
            </div>
        </div>
    </div>


    <script type="text/x-template" id="x-tmpl">
        <div class='col-sm-3 preview-wrp js-preview-wrp mb-2'>
            <div class='inner-wrap'>
                <div class='btn-action position text-center'>posicionar</div>
                <div class='imgwrp d-flex align-items-center justify-content-center'>
                    <img class='preview' src=''>
                </div>
                <div class="container">
                    <div class='row actions text-center cf'>
                        <div class='col btn-action remove'>remover</div>
                        <div class='col btn-action crop btn-crop-open'>recortar</div>
                    </div>
                </div>
            </div>
        </div>
    </script>


    <div class="container less-gutters-wrp">
        <div class="row less-gutters preview-list-wrp js-preview-list-wrp">
        {{--
            <div class='col-sm-3 preview-wrap mb-2'>
                <div class='inner-wrap'>
                    <div class='btn-action position text-center'>posicionar</div>
                    <div class='imgwrp d-flex align-items-center justify-content-center'>
                        <img class='preview' src='{{ asset('assets/uploads/one-sm.jpg') }}'>
                    </div>
                    <div class="container">
                        <div class='row actions text-center cf'>
                            <div class='col btn-action remove'>remover</div>
                            <div class='col btn-action crop'>recortar</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class='col-sm-3 preview-wrap mb-2'>
                <div class='inner-wrap'>
                    <div class='btn-action position text-center'>posicionar</div>
                    <div class='imgwrp d-flex align-items-center justify-content-center'>
                        <img class='preview' src='{{ asset('assets/uploads/two-sm.jpg') }}'>
                    </div>
                    <div class="container">
                        <div class='row actions text-center cf'>
                            <div class='col btn-action remove'>remover</div>
                            <div class='col btn-action crop'>recortar</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class='col-sm-3 preview-wrap mb-2'>
                <div class='inner-wrap'>
                    <div class='btn-action position text-center'>posicionar</div>
                    <div class='imgwrp d-flex align-items-center justify-content-center'>
                        <img class='preview' style='height: 70px;' src='{{ asset('assets/uploads/three-sm.jpg') }}'>
                    </div>
                    <div class="container">
                        <div class='row actions text-center cf'>
                            <div class='col btn-action remove'>remover</div>
                            <div class='col btn-action crop'>recortar</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class='col-sm-3 preview-wrap mb-2'>
                <div class='inner-wrap'>
                    <div class='btn-action position text-center'>posicionar</div>
                    <div class='imgwrp d-flex align-items-center justify-content-center'>
                        <img class='preview' style='width: 100%; height: 50px;' src='{{ asset('assets/uploads/one-sm.jpg') }}'>
                    </div>
                    <div class="container">
                        <div class='row actions text-center cf'>
                            <div class='col btn-action remove'>remover</div>
                            <div class='col btn-action crop'>recortar</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class='col-sm-3 preview-wrap mb-2'>
                <div class='inner-wrap'>
                    <div class='btn-action position text-center'>posicionar</div>
                    <div class='imgwrp d-flex align-items-center justify-content-center'>
                        <img class='preview' src='{{ asset('assets/uploads/two-sm.jpg') }}'>
                    </div>
                    <div class="container">
                        <div class='row actions text-center cf'>
                            <div class='col btn-action remove'>remover</div>
                            <div class='col btn-action crop'>recortar</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class='col-sm-3 preview-wrap mb-2'>
                <div class='inner-wrap'>
                    <div class='btn-action position text-center'>posicionar</div>
                    <div class='imgwrp d-flex align-items-center justify-content-center'>
                        <img class='preview' src='{{ asset('assets/uploads/three-sm.jpg') }}'>
                    </div>
                    <div class="container">
                        <div class='row actions text-center cf'>
                            <div class='col btn-action remove'>remover</div>
                            <div class='col btn-action crop'>recortar</div>
                        </div>
                    </div>
                </div>
            </div>
            --}}
        </div>
    </div>

    <div class="cropper-wrp js-cropper-wrp d-none row">
        <div class="crop-buttons col-12 col-md-2">
            <button class="btn btn-primary btn-crop-execute d-block mb-3"
                >Recortar</button>
            <button class="btn btn-success btn-crop-close d-block"
                >Cancelar</button>
        </div>
        {{-- Cropperjs requires a block element around the img element. --}}
        <div class="cropperjs-container js-cropperjs-container col-12 col-md-10">
            {{-- src attribute added through typescript --}}
            <img class="align-self-center" alt="cropperjs">
        </div>
        </div>
    </div>
@endsection

@section('page-scripts')
    <script
      type="text/javascript"
      src="{{ mix('assets/js/admin/images/main.js') }}"
    ></script>
@endsection
