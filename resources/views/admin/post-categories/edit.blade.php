@extends('admin.admin-layout')

@section('title', 'Cadastrar Nova Postagem')

@section('content')

<div class="crud post-categories edit">

    @php
        $btns['cancel']['display'] = true;
    @endphp

    @include('admin.shared.crud-actions-header', [
        'title' => "Editando categoria de post <em>{$post_category->name}</em>",
        'btns' => $btns
    ])

    <div class="crud-form-wrapper">
        {!! Form::model($post_category, ['method' => 'PATCH', 'route' => ['post-categories.update', $post_category->id]]) !!}
        @include('admin.post-categories.form', compact('btns'))
        {!! Form::close() !!}
    </div>

</div>

@endsection

