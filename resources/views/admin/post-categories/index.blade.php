@extends('admin.admin-layout')

@section('title', 'Lista de Postagens')

@section('content')

<div class="crud post-categories index">

    @php
        $btns['create']['display'] = true;
        $btns['create']['text'] = 'Nova';
        $btns['create']['route'] = route('post-categories.create');
    @endphp

    @include('admin.shared.crud-actions-header', [
        'title' => 'Listagem de categorias de publicações',
        'btns' => $btns
    ])

    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Slug</th>
                <th>Editar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($post_categories AS $post_category)
                <tr>
                    <td>{{ $post_category->id }}</td>
                    <td>{{ $post_category->name }}</td>
                    <td>{{ $post_category->slug }}</td>
                    <td>
                        <a href="{{ action('Admin\PostCategoriesController@edit', ['id' => $post_category->id]) }}">Editar</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection
