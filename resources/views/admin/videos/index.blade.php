@extends('admin.admin-layout')

@section('title', 'Lista de Videos')

@section('content')

<div class="crud videos-categories index">

    @php
        $btns['create']['display'] = true;
        $btns['create']['text'] = 'Novo';
        $btns['create']['route'] = route('videos.create');
    @endphp

    @include('admin.shared.crud-actions-header', [
        'title' => 'Listagem de Videos',
        'btns' => $btns
    ])

    @include('admin.shared.messages')

    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Título</th>
                <th>Posição</th>
                <th>Editar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($videos AS $video)
                <tr>
                    <td>{{ $video->id }}</td>
                    <td>{{ $video->title }}</td>
                    <td>{{ $video->pos }}</td>
                    <td>
                        <a href="{{ action('Admin\VideosController@edit', ['id' => $video->id]) }}">Editar</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

