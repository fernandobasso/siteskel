
<div class="form-group">
    {!! Form::label('title', 'Título') !!}
    {!! Form::text('title', null, [
      'class' => 'form-control',
      'placeholder' => 'O título é obrigatório',
      'required',
      'pattern=".{3,}"'
    ]) !!}
</div>

<div class="row">
  <div class="col-8">
    <div class="form-group">
        {!! Form::label('pos', 'Posição') !!}
        {!! Form::text('pos', null, [
          'class' => 'form-control',
          'placeholder' => '0 (opcional)',
          'pattern="\d+"',
          'style="width: 120px"'
        ]) !!}
        <div class="text-muted text-small mt-1" style="line-height: 1;">
          <small>
            Posição "1" faz o video aparecer como o primeiro, "2" como o segundo, etc.
            "0" é "sem posição específica (lista descendente por data de cadastro).
          </small>
        </div>
    </div>
  </div>
  <div class="col-4">
    <div class="js-video-preview-wrp">
      Insira um link para visualizar o vídeo.
      {{-- js includes video here --}}
    </div>
  </div>
</div>

<div class="form-group">
  {!! Form::label('link', 'Link') !!}
  {!! Form::text('link', null, [
    'class' => 'form-control js-video-link',
    'placeholder="http://... (obrigatório)"',
    'required',
    'pattern="http.{2,}"'
  ]) !!}
  <div class="text-muted text-small mt-1" style="max-width: 99%; line-height: 1;">
    <small>
      Insira um link do Youtube e pressione TAB ou clique em outro lugar da página.
    </small>
  </div>
</div>
<div class="form-group">
    {!! Form::label('intro', 'Descrição') !!}
    {!! Form::textarea('intro', null, [
      'class' => 'form-control',
      'placeholder' => 'Este campo é opcional...',
      'size' => 'x3']
    ) !!}
</div>

{!! Form::submit($btns['submit']['text'], ['class' => 'btn btn-primary']) !!}

@section('page-scripts')
    <script type="text/javascript" src="{{ asset('assets/js/admin/videos/videos.js') }}?{{ time() }}"></script>
@endsection
