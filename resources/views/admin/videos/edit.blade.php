@extends('admin.admin-layout')

@section('title', 'Editar Vídeo')

@section('content')

<div class="crud video edit">

    @php
        $btns['cancel']['display'] = true;
        $btns['cancel']['text'] = 'Cancelar';
        $btns['submit']['text'] = 'Gravar';
    @endphp

    @include('admin.shared.crud-actions-header', [
        'title' => "Editando vídeo <em>{$video->title}</em>",
        'btns' => $btns
    ])

    <div class="crud-form-wrapper">
        {!! Form::model($video, ['method' => 'PATCH', 'route' => ['videos.update', $video->id]]) !!}
        @include('admin.videos.form', compact('btns'))
        {!! Form::close() !!}
    </div>

</div>

@endsection


