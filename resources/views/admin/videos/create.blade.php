@extends('admin.admin-layout')

@section('title', 'Cadastrar Novo Vídeo')

@section('content')

<div class="crud video create">

    @php
        $btns['cancel']['display'] = true;
        $btns['cancel']['text'] = 'Cancelar';
        $btns['submit']['text'] = 'Gravar';
    @endphp

    @include('admin.shared.crud-actions-header', [
        'title' => 'Cadastrando novo vídeo',
        'btns' => $btns
    ])

    <div class="crud-form-wrapper">
        {!! Form::open(['action' => 'Admin\VideosController@store']) !!}
        @include('admin.videos.form', compact('btns'))
        {!! Form::close() !!}
    </div>
</div>

@endsection
