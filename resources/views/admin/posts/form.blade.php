
<div class="form-group">
    {!! Form::label('Categoria', 'Categoria') !!}
    {!! Form::select(
        'post_category_id',
        $post_categories,
        null,
    ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('title', 'Título') !!}
    {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<div class="form-group">
    {!! Form::label('intro', 'Introdução') !!}
    {!! Form::textarea('intro', null, ['class' => 'form-control', 'size' => 'x3']) !!}
</div>

<div class="form-group">
    {!! Form::label('text', 'Texto') !!}
    {!! Form::textarea('text', null, ['class' => 'form-control js-ck-editor-post']) !!}
</div>

{!! Form::submit('Gravar', ['class' => 'btn btn-primary']) !!}

<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<script>
    // https://stackoverflow.com/questions/20645750/ckeditor-turn-off-html-encoding
    CKEDITOR.replace('text', {
        language: 'pt-br',
        basicEntities: false,
        entities: false,
        htmlEncodeOutput: false
    });
</script>
