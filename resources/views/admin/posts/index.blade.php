@extends('admin.admin-layout')

@section('title', 'Lista de Postagens')


@section('content')

<div class="crud posts index">

    @php
        $btns['create']['display'] = true;
        $btns['create']['text'] = 'Novo';
        $btns['create']['route'] = route('posts.create')
    @endphp
    @include('admin.shared.crud-actions-header', [
        'title' => 'Listagem de posts',
        'btns' => $btns
    ])

    @include('admin.shared.messages')

    @include('admin.posts.search')

    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Título</th>
                <th>Categoria</th>
                <th>Imagens</th>
                <th>Editar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($posts AS $post)
                <tr>
                    <td>{{ $post->id }}</td>
                    <td>{{ $post->title }}</td>
                    <td>{{ $post->postCategory->name }}</td>
                    <td>
                        <a
                            href="{{ action('Admin\ImagesController@create', ['post_id' => $post->id]) }}"
                            title="Imagens">
                            <i class="far fa-images"></i>
                            ({{ $post->images_count }})
                        </a>
                    </td>
                    <td>
                        <a
                          href="{{ action('Admin\PostsController@edit', ['id' => $post->id]) }}"
                          title="Editar">
                          <i class="far fa-edit"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

</div>

@endsection

