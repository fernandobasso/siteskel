<div class="search border bg-light p-3 mb-4">
  <form
    method="get"
    class="index-search attendee-search justify-content-center">

    <div class="row mb-2">
      <div class="col-6">
        <div class="form-group row">
          <label class="col-form-label col-sm-3" for="category">
            Categoria:
          </label>
          <select
            class="category form-control col-sm-9"
            name="category"
            id="category"
          >
            <option value="">Selecione...</option>
            @foreach ($categories as $category)
              <option
                value="{{ $category->id }}"
                @if (request()->query('category') == $category->id)
                  selected
                @endif
              >
                {{ $category->name }}
              </option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="col-6">
        <div class="form-check form-group row mt-1 ml-5">
          <input
            class="form-check-input"
            type="checkbox"
            name="status"
            id="inactive-posts"
            value="1"
            @if (request()->query('status') == 1)
              checked
            @endif>
          <label class="form-check-label" for="inactive-posts">
            Postagens Inativas
          </label>
        </div>
      </div>
    </div>

    <div class="form-row">
      <div class="col-8">
        <input
          class="form-control"
          type="text"
          name="search_term"
          value="{{ request('search_term') }}"
          placeholder="Pesquisa por título">
      </div>

      <div class="col-4">
        <button
          class="btn btn-outline-primary"
          type="submit"
          value="Pesquisar"
        >
          <i class="fas fa-search"></i>
          Pesquisar
        </button>
        <a
          class="btn btn-outline-success"
          href="{{ route('posts.index') }}"
          title="Limpar a busca">
          <i class="fas fa-broom"></i>
          Limpar
        </a>
      </div>
      <div class="col-2">
      </div>
    </div>
  </form>

</div>
