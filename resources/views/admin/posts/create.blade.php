@extends('admin.admin-layout')

@section('title', 'Cadastrar Nova Postagem')

@section('content')

<div class="crud posts edit">

    @php
        $btns['cancel']['display'] = true;
    @endphp
    @include('admin.shared.crud-actions-header', [
        'title' => 'Cadastrando novo post',
        'btns' => $btns
    ])

    <div class="crud-form-wrapper">
        {!! Form::open(['action' => 'Admin\PostsController@store']) !!}
        @include('admin.posts.form')
        {!! Form::close() !!}
    </div>

</div>

@endsection

