<header class="crud-header crud-actions">
    <div class="row">
        <div class="col-8">
          <h3 class="h5">{!! $title !!}</h3>
        </div>
        <div class="col-4 ">
            @if ($btns && isset($btns['cancel']) && isset($btns['cancel']['display']))
            <a class="btn btn-outline-primary float-right"
                role="button"
                href="{{ url()->previous() }}">
                {{ $btns['cancel']['text'] }}
            </a>
            @endif

            @if ($btns && isset($btns['create']) && isset($btns['create']['display']) && $btns['create']['display'])
            <a class="btn btn-outline-primary float-right mr-2"
                role="button"
                href="{{ $btns['create']['route'] }}">
                {{ $btns['create']['text'] }}
            </a>
            @endif
        </div>
    </div>
</header>
