@if (session('success'))
  <div class="alert alert-success text-center">
    {!! session('success') !!}
  </div>
@endif

@if (session('danger'))
  <div class="alert alert-danger text-center">
    {!! session('danger') !!}
  </div>
@endif

@if (session('warning'))
  <div class="alert alert-warning text-center">
    {!! session('warning') !!}
  </div>
@endif

@if (session('info'))
  <div class="alert alert-info text-center">
    {!! session('info') !!}
  </div>
@endif

