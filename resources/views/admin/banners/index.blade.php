@extends('admin.admin-layout')

@section('title', 'Banners')

@section('content')

  @php
    $btns['cancel']['display'] = true;
    $btns['cancel']['text'] = 'Voltar';
  @endphp
  @include('admin.shared.crud-actions-header', [
    'title' => "Banners",
    'btns' => $btns
  ])

    <div class="container mb-3">
      <div class="row banner-form-wrp p-3">
        <div class="col">
          <form
            action="{{ route('admin.banners.store') }}"
            method="post"
            enctype="multipart/form-data"
            >
            {{ csrf_field() }}

            <input
              type="hidden"
              name="banner-width"
              value="{{ env('BANNER_WIDTH') }}"
            />

            <input
              type="hidden"
              name="banner-height"
              value="{{ env('BANNER_HEIGHT') }}"
            />

            <div class="form-group row">
              <div class="col-12 mb-4">
                <label for="banner">
                  <i class="fa fa-upload"></i>
                  <span>Selecione uma imagem</span>
                  <span class="banner-dimensions">
                    <tt>[{{ env('BANNER_WIDTH') }}x{{ env('BANNER_HEIGHT') }}]</tt>
                  </span>
                </label>
                <input
                  id="banner"
                  name="banner"
                  type="file"
                  required
                />
              </div>
              <div class="js-banner-dimensions-validation alert alert-danger col-12 d-none"></div>
              <div class="banner-upload-preview"></div>
            </div>

            <div class="form-group row">
              <label class="col col-md-2 col-form-label" for="title">Título</label>
              <div class="col col-md-10">
                <input
                  class="form-control"
                  type="text"
                  name="title"
                  placeholder="Título / Texto Principal"
                  required
                  pattern=".+"
                />
              </div>
            </div>

            <div class="form-group row">
              <label class="col col-md-2 col-form-label" for="subtitle">Descrição</label>
              <div class="col col-md-10">
                <input
                  class="form-control"
                  type="text"
                  name="subtitle"
                  placeholder="Subítulo / Texto Secundário"
                />
              </div>
            </div>

            <div class="form-group row">
              <label class="col col-md-2 col-form-label" for="subtitle">Link</label>
              <div class="col col-md-10">
                <input
                  class="form-control"
                  type="text"
                  name="link"
                  placeholder="http://exemplo.com.br/alguma-pagina"
                />
              </div>
            </div>

            <div class="form-group row">
              <div class="col col-6">
                <div class="row">
                  <label class="col col-md-4 col-form-label" for="position">Posição</label>
                  <div class="col col-md-4">
                    <input
                      class="form-control"
                      type="text"
                      name="pos"
                      required
                      pattern="\d+"
                    />
                  </div>
                </div>
              </div>

              <div class="col col-6">
                <div class="row">
                  <label class="col col-md-4 form-check-label" for="status">Ativo?</label>
                  <div class="col col-md-4">
                    <input
                      id="status"
                      class="form-check-input"
                      type="checkbox"
                      name="status"
                      checked
                    />
                  </div>
                </div>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-6">
                <input
                  class="form-control btn btn-primary"
                  type="submit"
                  value="Upload" />
              </div>
            </div>
          </form>

        </div>
      </div>

      @foreach ($banners as $banner)
      <div
        class="row banner-index-wrp js-banner-index-wrp p-3"
        data-banner-id="{{ $banner->id }}"
      >
        <div class="col">
          <form
            action="{{ route('admin.banners.update') }}"
            method="post"
            enctype="multipart/form-data"
          >
            {{ csrf_field() }}

            <div class="form-group row">
              <div class="banner-preview col">
                <img
                  class="banner-img"
                  src="{{ url($banner->path()) }}"
                  alt="{{ $banner->title }}"
                />
              </div>
            </div>

            <input
              type="hidden"
              name="_method"
              value="patch"
            />

            <input
              type="hidden"
              name="id"
              value="{{ $banner->id }}"
            />

            <div class="form-group row">
              <label class="col col-md-2 col-form-label" for="title">Título</label>
              <div class="col col-md-10">
                <input
                  class="form-control"
                  type="text"
                  name="title"
                  value="{{ $banner->title }}"
                  required
                  pattern=".+"
                />
              </div>
            </div>

            <div class="form-group row">
              <label class="col col-md-2 col-form-label" for="subtitle">Descrição</label>
              <div class="col col-md-10">
                <input
                  class="form-control"
                  type="text"
                  name="subtitle"
                  value="{{ $banner->subtitle }}"
                />
              </div>
            </div>

            <div class="form-group row">
              <label class="col col-md-2 col-form-label" for="subtitle">Link</label>
              <div class="col col-md-10">
                <input
                  class="form-control"
                  type="text"
                  name="link"
                  value="{{ $banner->link }}"
                  placeholder="http://exemplo.com.br/alguma-pagina"
                />
              </div>
            </div>

            <div class="form-group row">
              <div class="col col-6">
                <div class="row">
                  <label class="col col-md-4 col-form-label" for="position">Posição</label>
                  <div class="col col-md-4">
                    <input
                      class="form-control"
                      type="text"
                      name="pos"
                      value="{{ $banner->pos }}"
                    />
                  </div>
                </div>
              </div>

              <div class="col col-6">
                <div class="row">
                  <label class="col col-md-4 form-check-label" for="status">Ativo?</label>
                  <div class="col col-md-4">
                    <input
                      id="status"
                      class="form-check-input"
                      type="checkbox"
                      name="status"
                      @if ($banner->status == 1)
                        checked
                      @endif
                    />
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group row wrp-actions">
              <div class="col-6">
                <input
                  class="form-control btn btn-primary"
                  type="submit"
                  value="Atualizar" />
              </div>
              <div class="col-6">
                <div class="row wrp-btns">
                  <button
                    data-id={{ $banner->id }}
                    class="col-12 js-btn-destroy btn-destroy show form-control btn btn-danger"
                  >
                    Excluír
                    <i class="fa fa-trash-alt"></i>
                  </button>
                  <div class="js-wrp-btns-destroy col-12 wrp-btns-destroy hide">
                    <div class="row">
                      <button
                        class="col-6 js-btn-destroy-cancel btn-destroy-cancel form-control btn btn-outline-primary"
                      >
                        Cancelar
                        <i class="fas fa-long-arrow-alt-left"></i>
                      </button>
                      <button
                        data-id={{ $banner->id }}
                        data-route={{ action('Admin\BannersController@destroy', ['id' => $banner->id]) }}
                        class="col-6 js-btn-destroy-confirm btn-destroy-confirm form-control btn btn-danger"
                      >
                        Sim, excluír!
                        <i class="fa fa-trash"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>

        </div>
      </div>
      @endforeach


    </div>

    @endsection

    @section('page-scripts')
      <script
        type="text/javascript"
        src="{{ mix('assets/js/admin/banners/banners.js') }}">
      </script>
    @endsection

