<div style="color: #006e3f">
  <p>Nome: {{ $data['name'] }}</p>
  <p>E-mail: {{ $data['email'] }}</p>
  <p>Fone: {{ $data['phone'] }}</p>
  <p>
    <strong>IMPORTANTE:</strong> Não esqueça sua senha de acesso:
    <strong><tt style="font-size: 18px">{{ $data['password'] }}</tt></strong>
  </p>
</div>
