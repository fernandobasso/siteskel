# SiteSkel

## Local Dev

Create the database. Replace host, user, password and the DB name appropriately.

```
mysql \
  --host=localhost \
  --user=devel \
  --password=1234 \
  <<< 'DROP DATABASE siteskel_dev' \
  ;

mysql \
  --host=localhost \
  --user=devel \
  --password=1234 \
  <<<'CREATE DATABASE siteskel_dev CHARSET utf8mb4 COLLATE utf8mb4_unicode_ci;' \
  ;
```

Then run the migrations and seeds.

```
composer dump-autoload
php artisan migrate
php artisan db:seed
```

NOTE: Seeding may fail if are you _re-seeding_ and end up trying to re-insert
unique-valued data. Examples would be PKs or emails which must be unique.

Connect to the local MariaDB server:

```
mysql -h localhost -u devel -p1234 -D siteskel_dev --auto-rehash;
```

Run the local dev server:

```
php artisan serve --port 7999
```


## Font Awesome

Font Awesome is compiled as a separate file (not included from the main css file).


## Compiling Assets

Compile SCSS, JavaScript and TypeScript with Laravel Mix:

```
npm run watch
```

NOTE: Everything in `public/assets/` is what Laravel Mix copies from `resources/assets/`.

## Deployment In Production

```
mkdir -pv storage/framework/{cache,sessions,views}
```

## Scroll Feature

Some menus do not navigate to a different page but instead just scroll the viewport to the element with a given #id. For it to work, a link must have the class `scroll-to`, and a data attribute `data-scroll-to` whose value is the id of the element to scroll to. Ex:

```html
<a href="https://dev.siteskel.me" class="scroll-to" data-scroll-to="#row-services">Services</a>
```

And then, the home page should have an element (like a section, article or div) with the id `row-services`. In this case, the services element is on the home page. Therefore, the `href` has the value `https://dev.siteskel.me`. If the element is on another page, just make the `href` attribute point to that page, and the _scroll to_ feature will work just fine too.

```html
<a href="https://dev.siteskel.me/about-us" class="scroll-to" data-scroll-to="#row-our-mission">Our Mission</a>
```

And then on the page/url "about-us", make sure to have an element with the id `row-our-mission`.

NOTE: The `data-scroll-to` attribute must contain a leading `#` for the value.
