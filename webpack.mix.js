let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


//
// SITE
//
mix.sass(
    'resources/assets/scss/site/bootstrap-custom-site.scss',
    'public/assets/css/site'
).sourceMaps().version();

mix.sass(
    'resources/assets/scss/site/main-site.scss',
    'public/assets/css/site'
).sourceMaps().version();

mix.js(
    'resources/assets/js/site/main-site.js',
    'public/assets/js/site'
).sourceMaps().version();

//
// ADMIN
//
mix.sass(
    'resources/assets/scss/admin/main-admin.scss',
    'public/assets/css/admin'
).sourceMaps().version();

mix.sass(
    'resources/assets/scss/admin/bootstrap-custom-admin.scss',
    'public/assets/css/admin'
).sourceMaps().version();

mix.js('resources/assets/js/admin/main-admin.js',
    'public/assets/js/admin'
).sourceMaps().version();

mix.js('resources/assets/js/admin/videos/videos.js',
    'public/assets/js/admin/videos'
).sourceMaps().version();

mix.js('resources/assets/js/admin/images/main.js',
    'public/assets/js/admin/images'
).sourceMaps().version();

mix.js(
  'resources/assets/js/admin/banners/banners.js',
  'public/assets/js/admin/banners'
).sourceMaps().version();


//
// SHARED
//
mix.sass(
    'resources/assets/fa47/scss/font-awesome.scss',
    'public/assets/fa47'
);

mix.copyDirectory(
  'resources/assets/images',
  'public/assets/images',
);

